//
//  CartPage.swift
//  Template3
//
//  Created by TISSA Technology on 2/19/21.
//

import UIKit
import SideMenu
import Alamofire
import SDWebImage

class CartPage: UIViewController {
    @IBOutlet weak var cartTable: UITableView!
    @IBOutlet weak var checkoutBtn: UIButton!

    var CartProducts = NSArray()
    var totatpro = NSString()
    var listproductId = String()
    var totalListSum  = String()
    var getsequenceId  = String()
    var cartitemIdStr  = String()
    var quantityInt  = Int()
    var cartIdStr  = String()
    
   
    var qtycount = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cartTable.register(UINib(nibName: "CartCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        GetCartItems()
        
        cartTable.rowHeight = UITableView.automaticDimension
        cartTable.estimatedRowHeight = 128
        
    }
    
    @IBAction func backClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
   
    }
    
    @IBAction func exploreBtnClicked(_ sender: Any) {
        
        let explore = self.storyboard?.instantiateViewController(withIdentifier: "ExploremenuPage") as! ExploremenuPage
        self.navigationController?.pushViewController(explore, animated: true)
        
    }
    
    @IBAction func checkoutClicked(_ sender: Any) {
        
        print("Total Amt - \(totalListSum)")

        let amount = Double(totalListSum);

        if amount == nil {

            self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")

        }else{

        if amount!  == 0 {

            showSimpleAlert(messagess: "No items available in the cart to place order")

        }else{

        let defaults = UserDefaults.standard

        defaults.set(totalListSum, forKey: "totalcartPrice")

        defaults.set(CartProducts, forKey: "cartarray")


            let address = self.storyboard?.instantiateViewController(withIdentifier: "AddressPage") as! AddressPage
            self.navigationController?.pushViewController(address, animated: true)
            
       }

        }
       
    }
    

}
// MARK: - uiTableViewDatasource
extension CartPage: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return CartProducts.count
         

    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CartCell
        cell.selectionStyle = .none
        
        cell.oterview.layer.cornerRadius = 8
        cell.oterview.layer.shadowColor = UIColor.lightGray.cgColor
        cell.oterview.layer.shadowOpacity = 1
        cell.oterview.layer.shadowOffset = .zero
        cell.oterview.layer.shadowRadius = 3
        cell.deleteBtn.layer.cornerRadius = 8
        cell.deleteBtn.layer.borderWidth = 0.5
        cell.deleteBtn.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(DeleteBtnPredded(_:)), for: .touchUpInside)
        
//        if expand[indexPath.row] == "yes" {
//            cell.customView.isHidden = false
//        }else{
//            
//            cell.customView.isHidden = true
//        }
        
        cell.minusBtn.tag = indexPath.row
        cell.minusBtn.addTarget(self, action: #selector(QuantityminusClicked(_:)), for: .touchUpInside)
        
        cell.plusBtn.tag = indexPath.row
        cell.plusBtn.addTarget(self, action: #selector(QuantityplusClicked(_:)), for: .touchUpInside)
        
        let dictObj = self.CartProducts[indexPath.row] as! NSDictionary

       cell.dishname.text = dictObj["product_name"] as? String

       let qnt = dictObj["quantity"] as! Int
       cell.QtyLbl.text = String(qnt)

           let rupee = "$"

           let pricedata = dictObj["line_total"]as! String

          cell.dishprice.text = rupee + pricedata
        
        var urlStr = String()

        if dictObj["product_url"] is NSNull {
            urlStr = ""
        }else{

            urlStr = dictObj["product_url"] as! String

        }

        let url = URL(string: urlStr )

       cell.dishimage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.dishimage.sd_setImage(with: url) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                cell.dishimage.image = UIImage(named: "noimage.png")
            } else {
                // Successful in loading image
                cell.dishimage.image = image
            }
        }
        
        
        
                let list:NSArray = dictObj.value(forKey: "ingredient") as! NSArray
        
                if list.count == 0 {
        
                    cell.IngDataLbl.isHidden = true
                   // cell.ingredientTitL.isHidden = true
                    cell.customView.isHidden = true
                    cell.boxheight.constant = 0
                }else{
                    cell.IngDataLbl.isHidden = false
                   // cell.ingredientTitL.isHidden = false
                    cell.customView.isHidden = false
                    cell.boxheight.constant = 79
                    
                    var indnamearr:[String] = []
                    var indpricearr:[Double] = []
                    var indpricearrforshow:[String] = []
        
                    for (index, element1) in list.enumerated() {
        
                        let dictObj1 = list[index] as! NSDictionary
                        let ingredprice = dictObj1["line_total"]as! String
                        let ingredientname = dictObj1["ingredient_name"]as! String
                        let quantitystrrr = dictObj1["quantity"]as! Int
                        let convertqty = Int(quantitystrrr)
        
                        let nameshow = "\(index+1).\(ingredientname):\nPrice :$\(ingredprice)\nQty:\(convertqty)\n\n"
        
                        indpricearrforshow.append(nameshow)
                        indnamearr.append(ingredientname)
                        indpricearr.append(Double(ingredprice)!)
                    }
        
                    let total = indpricearr.reduce(0, +)
        
                    print("ingredient total - \(total)")
        
                    let rupee = "$"
        
                               let pricedata = dictObj["line_total"]as! String
        
                               let Pamt = Double(pricedata)
        
                               let Tamt =  Double(Pamt!) + total
        
        
                              cell.dishprice.text = rupee + String(format: "%.2f", Tamt)
        
            cell.IngDataLbl.text = indpricearrforshow.map { String($0) }.joined(separator: "")
        
                }
        
        
        return cell
    }
    
    @IBAction func DeleteBtnPredded(_ sender: UIButton) {
           
            let index = sender.tag
           
           let dictObj = self.CartProducts[index] as! NSDictionary
           
           let delpro = dictObj["product_id"] as! Int
           let delseq = dictObj["sequence_id"] as! Int
           
           self.listproductId = String(delpro)
           self.getsequenceId = String(delseq)
           
           showSimpleAlert1()
        
    }
    
    func showSimpleAlert1() {
           let alert = UIAlertController(title: nil, message: "Are you sure you want to delete?",         preferredStyle: UIAlertController.Style.alert)

           alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.default, handler: { _ in
               //Cancel Action//
           }))
           alert.addAction(UIAlertAction(title: "YES",
                                         style: UIAlertAction.Style.default,
                                         handler: {(_: UIAlertAction!) in
                                           
                                            ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                            self.DeleteCartItems()
                                            
           }))
           self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
       }
    
    
    @IBAction func QuantityminusClicked(_ sender: UIButton) {

        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = cartTable.cellForRow(at: indexPath) as! CartCell
        
        let getqty = cell.QtyLbl.text!
        let convertqty = Int(getqty)
        
        if convertqty! > 1 {
         
        quantityInt  = convertqty! - 1
    
         let index = sender.tag
        
        let dictObj = self.CartProducts[index] as! NSDictionary
        
        let delpro = dictObj["product_id"] as! Int
        let delseq = dictObj["sequence_id"] as! Int
        let cartiddStr = dictObj["cart_id"] as! Int
        let cartitemiddStr = dictObj["cart_item_id"] as! Int
        
        self.listproductId = String(delpro)
        self.getsequenceId = String(delseq)
        self.cartIdStr = String(cartiddStr)
        self.cartitemIdStr = String(cartitemiddStr)
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        updateToCart()
            
        }else{
            
            
        }
    }
    
    @IBAction func QuantityplusClicked(_ sender: UIButton) {

        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = cartTable.cellForRow(at: indexPath) as! CartCell
        
        let getqty = cell.QtyLbl.text!
        let convertqty = Int(getqty)
        quantityInt  = convertqty! + 1
       
        
         let index = sender.tag
        
        let dictObj = self.CartProducts[index] as! NSDictionary
        
        let delpro = dictObj["product_id"] as! Int
        let delseq = dictObj["sequence_id"] as! Int
        let cartiddStr = dictObj["cart_id"] as! Int
        let cartitemiddStr = dictObj["cart_item_id"] as! Int
        
        self.listproductId = String(delpro)
        self.getsequenceId = String(delseq)
        self.cartIdStr = String(cartiddStr)
        self.cartitemIdStr = String(cartitemiddStr)
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        updateToCart()
        
    }
    
    
    
   
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
            return  UITableView.automaticDimension
        
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//        let detail = self.storyboard?.instantiateViewController(withIdentifier: "menuDetailPage") as! menuDetailPage
//        self.navigationController?.pushViewController(detail, animated: true)
//        
//    }
    
}

// MARK: - Api LIst
extension CartPage {
    
    //MARK: Webservice Call Search product by category
        
        
        func GetCartItems(){
            
            let defaults = UserDefaults.standard
            
          //  let admintoken = defaults.object(forKey: "adminToken")as? String
            
            let admintoken = defaults.object(forKey: "custToken")as? String
            
            let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
            
            let cartidStr = String(avlCartId)
            
            let customerid = defaults.integer(forKey: "custId")

            let customeridStr = String(customerid)
            
            let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
          //  let trimmedString = categoryStr.removingAllWhitespaces()
            
            let urlString = GlobalClass.DevlopmentApi+"cart-item/?cart_id=\(avlCartId)&status=ACTIVE&restaurant_id=\(GlobalClass.restaurantGlobalid)"
            
           

                
            print("cartproducts get url - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho,
                    "user_id": customeridStr,
                    "cart_id": cartidStr,
                    "action": "cart-item"
                ]

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                    print(response)

                                    if response.response?.statusCode == 200{
                                     
                                     let dict :NSDictionary = response.value! as! NSDictionary
                                     
                                        
                                           let list:NSArray = dict.value(forKey: "results") as! NSArray
                                        
                                        if list.count == 0 {
                                            
                                            ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                           self.CartProducts = []
                                            self.cartTable.reloadData()

                                            let rupee = "Checkout  $"
                                            let sum = "0"
                                                
                                    let title = rupee + sum
                                                
                                self.checkoutBtn.setTitle(title, for: UIControl.State.normal)
                                            
                                            self.totalListSum = "0"
                                            self.showSimpleAlert(messagess:"No items avaible in cart")
                                        }else{
                                        
                                            self.CartProducts = dict.value(forKey: "results")as! NSArray
                                            
                                       let ttl = dict.value(forKey: "total_cost") as! String
                                            
                                            self.totalListSum = ttl
                                            
                                            let rupee = "Checkout  $"
                                            let title = rupee + ttl
                                            
                                self.checkoutBtn.setTitle(title, for: UIControl.State.normal)
                                            
                                             print("Total Amt - \(ttl)")
                                             print("cart products list - \(self.CartProducts)")
                                            
                                            self.cartTable.reloadData()

                                            
                                        }
                                          
                                        ERProgressHud.sharedInstance.hide()

                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    
                                    if error.localizedDescription == msg {
                                        
                                        self.showSimpleAlert(messagess:"No internet connection")
                                        
                                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                        
                                        self.showSimpleAlert(messagess:"Slow Internet Detected")
                                                
                                            }else{
                                            
                                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                            }

                                               print(error)
                                        }
                }
                
          
                
            }
        

    
    
    //MARK: Webservice Call delete Cart
    
    func DeleteCartItems(){
        
        let defaults = UserDefaults.standard
        
       // let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)

        let customerid = defaults.integer(forKey: "custId")

        let customeridStr = String(customerid)
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
      //  let trimmedString = categoryStr.removingAllWhitespaces()
        
        let urlString = GlobalClass.DevlopmentApi+"cart-item/\(avlCartId)/?product_id=\(listproductId)&sequence_id=\(getsequenceId)"
        
       

            
        print("cartproducts get url - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "cart-item"
            ]

        AF.request(urlString, method: .delete, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                    
                                    
                                    
                                    self.GetCartItems()
                                  print("success")
                                    
                                    
                                }else{
                                    
                                     if response.response?.statusCode == 401{
                                 
                                        ERProgressHud.sharedInstance.hide()
                                        self.SessionAlert()
                                        
                                    }
                                    
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                                    self.showSimpleAlert(messagess:"Slow Internet Detected")
                                            
                                        }else{
                                        
                                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                        }

                                           print(error)
                                    }
            }
            
      
            
        }
    
    //MARK: Webservice Call for update to cart

     
    func updateToCart() {
       
        let defaults = UserDefaults.standard
        
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        if defaults.object(forKey: "AvlbCartId") == nil {
          
            self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
           
            
        }else{
        
         let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
            let cartidStr = String(avlCartId)
            
       // let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let admintoken = defaults.object(forKey: "custToken")as? String
            
            print("customer token -\(String(describing: admintoken))")
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

        let urlString = GlobalClass.DevlopmentApi+"cart-item/\(cartitemIdStr)/"
        
 
          
            let metadataDict = ["id":cartitemIdStr,"product_id":listproductId, "quantity":quantityInt,"ingredient_id":0,"sequence_id":getsequenceId,"cart":cartIdStr] as [String : Any]
          
            let productarr = [metadataDict]
            print("product element Dict - \(productarr)")
            
          
            
        
            let fileUrl = URL(string: urlString)

            var request = URLRequest(url: fileUrl!)
            request.httpMethod = "PUT"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(autho, forHTTPHeaderField: "Authorization")
            request.setValue(customeridStr, forHTTPHeaderField: "user_id")
            request.setValue(cartidStr, forHTTPHeaderField: "cart_id")
            request.setValue("cart-item", forHTTPHeaderField: "action")


            request.httpBody = try! JSONSerialization.data(withJSONObject: metadataDict)

            AF.request(request)
                .responseJSON { response in
                    // do whatever you want here
                    switch response.result {
                                            case .success:
                                                print(response)
                    
                                                if response.response?.statusCode == 200{
                    
                                                    self.GetCartItems()
                    
                                                }else{
                    
                                                    if response.response?.statusCode == 401{
                    
                                                        ERProgressHud.sharedInstance.hide()
                                                        self.SessionAlert()
                    
                                                    }else if response.response?.statusCode == 500{
                    
                                                        ERProgressHud.sharedInstance.hide()
                    
                                                        let dict :NSDictionary = response.value! as! NSDictionary
                    
                                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                    }else{
                    
                                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                       }
                    
                                                }
                    
                                                break
                                            case .failure(let error):
                                                ERProgressHud.sharedInstance.hide()
                    
                                                print(error.localizedDescription)
                    
                                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                    
                                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                    
                                                let msgrs = "URLSessionTask failed with error: The request timed out."
                    
                                                if error.localizedDescription == msg {
                    
                                            self.showSimpleAlert(messagess:"No internet connection")
                    
                                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                    
                                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                    
                                                }else{
                    
                                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                                }
                    
                                                   print(error)
                                            }
            }
            
  
    }

     }
    
    
}

// MARK: - AlertController
extension CartPage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}
