//
//  ingredientCell.swift
//  Template3
//
//  Created by TISSA Technology on 2/19/21.
//

import UIKit

class ingredientCell: UICollectionViewCell {
    @IBOutlet weak var ingredientImage: UIImageView!
    @IBOutlet weak var ingredientName: UILabel!
    @IBOutlet weak var ingredientPrice: UILabel!
    @IBOutlet weak var selectedview: UIView!
    @IBOutlet weak var tickimage: UIImageView!
    @IBOutlet weak var backview: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
