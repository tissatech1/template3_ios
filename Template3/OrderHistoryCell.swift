//
//  OrderHistoryCell.swift
//  Template3
//
//  Created by TISSA Technology on 2/24/21.
//

import UIKit

class OrderHistoryCell: UITableViewCell {

    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var outerview: UIView!
    @IBOutlet weak var ordernumberView: UIView!
    @IBOutlet weak var itemsLbl: UILabel!
    @IBOutlet weak var itemlistLbl: UILabel!
    @IBOutlet weak var orderplacedateLbkl: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var ordernumLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
