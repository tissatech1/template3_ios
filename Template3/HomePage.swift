//
//  HomePage.swift
//  Template3
//
//  Created by TISSA Technology on 2/15/21.
//

import UIKit
import SideMenu
import Alamofire
import SDWebImage
class HomePage: UIViewController {

    @IBOutlet weak var offercollectionView: UICollectionView!
    @IBOutlet weak var specialoffersCollectionView: UICollectionView!
    @IBOutlet weak var sellerCollectionView: UICollectionView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var subview1: UIView!
    @IBOutlet weak var subview2: UIView!
    @IBOutlet weak var takeawaybtn: UIButton!
    @IBOutlet weak var dineinBtn: UIButton!
    @IBOutlet weak var exview1: UIView!
    @IBOutlet weak var exview2: UIView!
    @IBOutlet weak var exview3: UIView!
    @IBOutlet weak var exview4: UIView!
    @IBOutlet weak var exview5: UIView!
    @IBOutlet weak var tabview: UIView!
    @IBOutlet weak var countview: UIView!
    @IBOutlet weak var cartcountLbl: UILabel!
    @IBOutlet weak var topaddress: UILabel!
    @IBOutlet weak var opcloseFlag: UILabel!

    let offerimageset = ["freshfood.jpeg","heathy.jpg","fastfood.jpeg","delicious.jpg"]
    
    var bestsellerList = NSArray()
    var OffersList = NSArray()
    var passproductid = Int()
    var quantityNumber = Int()
    var selectiontagArray: [String] = []
    var restStatusStr = String()

    
//    lazy var cellSizes: [CGSize] = {
//        var cellSizes = [CGSize]()
//
//        for _ in 0...100 {
//            let random = Int(arc4random_uniform((UInt32(100))))
//
//          //  cellSizes.append(CGSize(width: 140, height: 50 + random))
//            cellSizes.append(CGSize(width: 140, height: 140))
//        }
//
//        return cellSizes
//    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = UserDefaults.standard
        
        defaults.set("1", forKey: "clickedStoreId")
        
        let restid = defaults.integer(forKey: "clickedStoreId")
        GlobalClass.restaurantGlobalid = String(restid)
       
        topaddress.text = defaults.object(forKey: "citylocation")as? String
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        view1.layer.cornerRadius = 10
        subview1.layer.cornerRadius = 10
        subview2.layer.cornerRadius = 10
        
        exview1.layer.cornerRadius = 10
        exview2.layer.cornerRadius = 10
        exview3.layer.cornerRadius = 10
        exview4.layer.cornerRadius = 10
        exview5.layer.cornerRadius = 10

        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        
        
        let viewNib = UINib(nibName: "offercollectionCell", bundle: nil)
        offercollectionView.register(viewNib, forCellWithReuseIdentifier: "cell")
        
        let viewNib1 = UINib(nibName: "specialoffercollectionCell", bundle: nil)
        specialoffersCollectionView.register(viewNib1, forCellWithReuseIdentifier: "cell")
        
        let viewNib2 = UINib(nibName: "bestsellerCollectionCell", bundle: nil)
        sellerCollectionView.register(viewNib2, forCellWithReuseIdentifier: "cell1")
        
        tabview.layer.cornerRadius = 20
        tabview.layer.borderWidth = 2
        tabview.layer.borderColor = UIColor.white.cgColor
        countview.layer.cornerRadius = 8
       
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
           
           adminlogincheck()
            
        }else{
            
            checkCartAvl()
            gettiming()
           // GetMenuList()
            
        }
        
    }
    
    
    @IBAction func menuBtnClicked(_ sender: Any) {
        
        SideMenuManager.default.leftMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.leftMenuNavigationController!, animated: true, completion: nil)
    }
    
    
    @IBAction func exampleBtn(_ sender: Any) {
        
       
//        guard let popupVC = storyboard?.instantiateViewController(withIdentifier: "BottomView") as? BottomView else { return }
//        popupVC.height = self.view.frame.height/1.5
//        popupVC.topCornerRadius = 20
//        popupVC.presentDuration = 0.5
//        popupVC.dismissDuration = 0.5
//        popupVC.shouldDismissInteractivelty = true
//        popupVC.popupDelegate = self
//        present(popupVC, animated: true, completion: nil)
//
        let popupVC = (storyboard?.instantiateViewController(withIdentifier: "LocationViewlist") as? LocationViewlist)!
        
        present(popupVC, animated: true, completion: nil)
        
        
    }
    
    @IBAction func takeawayBtnClicked(_ sender: Any) {
        
        let image = UIImage(named: "button") as UIImage?
        self.takeawaybtn.setBackgroundImage(image, for: .normal)
        
        let image1 = UIImage(named: "dry-clean") as UIImage?
        self.dineinBtn.setBackgroundImage(image1, for: .normal)
    }
    
    
    @IBAction func dineinBtnClicked(_ sender: Any) {
        
        let image = UIImage(named: "dry-clean") as UIImage?
        self.takeawaybtn.setBackgroundImage(image, for: .normal)
        
        let image1 = UIImage(named: "button") as UIImage?
        self.dineinBtn.setBackgroundImage(image1, for: .normal)
    }
    
    @IBAction func exploremenuClicked(_ sender: Any) {
        
        let explore = self.storyboard?.instantiateViewController(withIdentifier: "ExploremenuPage") as! ExploremenuPage
        self.navigationController?.pushViewController(explore, animated: true)
        
    }
    
    
    @IBAction func offertabClicked(_ sender: Any) {
        
        let offer = self.storyboard?.instantiateViewController(withIdentifier: "OfferPage") as! OfferPage
        self.navigationController?.pushViewController(offer, animated: true)
    }
    
    @IBAction func orderhistoryClicked(_ sender: Any) {
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            
        }else{
        
        let order = self.storyboard?.instantiateViewController(withIdentifier: "OrderHistoryPage") as! OrderHistoryPage
        self.navigationController?.pushViewController(order, animated: true)
            
        }
    }
    
    @IBAction func carttabClicked(_ sender: Any) {
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            
        }else{
        
        if restStatusStr == "open" {
        
        let cart = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! CartPage
        self.navigationController?.pushViewController(cart, animated: true)
        
        }else{
            
        }
        }
   
    }
}
extension HomePage: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
       // print("SideMenu Appearing! (animated: \(animated))")
        self.view.alpha = 0.5
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Appeared! (animated: \(animated))")
        
        self.view.alpha = 0.5
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappearing! (animated: \(animated))")
        
        self.view.alpha = 1
    }
    
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappeared! (animated: \(animated))")
        
        self.view.alpha = 1
    }
}

extension HomePage: BottomPopupDelegate {
    
    func bottomPopupViewLoaded() {
        print("bottomPopupViewLoaded")
    }
    
    func bottomPopupWillAppear() {
        print("bottomPopupWillAppear")
    }
    
    func bottomPopupDidAppear() {
        print("bottomPopupDidAppear")
    }
    
    func bottomPopupWillDismiss() {
        print("bottomPopupWillDismiss")
        
        let defaults = UserDefaults.standard
        topaddress.text = defaults.object(forKey: "citylocation")as? String
    }
    
    func bottomPopupDidDismiss() {
        print("bottomPopupDidDismiss")
    }
    
    func bottomPopupDismissInteractionPercentChanged(from oldValue: CGFloat, to newValue: CGFloat) {
        print("bottomPopupDismissInteractionPercentChanged fromValue: \(oldValue) to: \(newValue)")
    }
}
// MARK: - UICollectionViewDataSource
extension HomePage: UICollectionViewDataSource,UICollectionViewDelegate {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == offercollectionView {
        
        return offerimageset.count
            
        }else if collectionView == specialoffersCollectionView{
            
            return OffersList.count
            
        }else if collectionView == sellerCollectionView{
         
        return bestsellerList.count
            
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        if collectionView == offercollectionView {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! offercollectionCell
            
                    cell.cellimages.layer.masksToBounds = false
                    cell.cellimages.layer.cornerRadius = 5
                    cell.cellimages.clipsToBounds = true
                    cell.cellimages.image = UIImage(named:offerimageset[indexPath.item])
            
            return cell
            
        }else if collectionView == specialoffersCollectionView{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! specialoffercollectionCell
            
            cell.celltwoimages.layer.masksToBounds = false
            cell.celltwoimages.layer.cornerRadius = 2
            cell.celltwoimages.clipsToBounds = true
           // cell.celltwoimages.image = UIImage(named:"bannerimage.jpeg")
          
            let dictObj = self.OffersList[indexPath.row] as! NSDictionary
           
            var urlStr = String()
            if dictObj["product_url"] is NSNull || dictObj["product_url"] == nil{

                        urlStr = ""

                    }else{
                        urlStr = dictObj["product_url"] as! String
                    }
                  
                    let url = URL(string: urlStr )


            cell.celltwoimages.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.celltwoimages.sd_setImage(with: url) { (image, error, cache, urls) in
                        if (error != nil) {
                            // Failed to load image
                            cell.celltwoimages.image = UIImage(named: "noimage.png")
                        } else {
                            // Successful in loading image
                            cell.celltwoimages.image = image
                        }
                    }
            
            
            return cell
            
        }else{
            
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath) as! bestsellerCollectionCell
            
            cell1.dishimage.layer.masksToBounds = false
            cell1.dishimage.layer.cornerRadius = 2
            cell1.dishimage.clipsToBounds = true
            
            let dictObj = self.bestsellerList[indexPath.row] as! NSDictionary
         //   let category = dictObj["category"]
         //   cell.itemsLable.text = (category as! String)
              
            var urlStr = String()
            if dictObj["product_url"] is NSNull || dictObj["product_url"] == nil{

                        urlStr = ""

                    }else{
                        urlStr = dictObj["product_url"] as! String
                    }
                  
                    let url = URL(string: urlStr )


            cell1.dishimage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell1.dishimage.sd_setImage(with: url) { (image, error, cache, urls) in
                        if (error != nil) {
                            // Failed to load image
                            cell1.dishimage.image = UIImage(named: "noimage.png")
                        } else {
                            // Successful in loading image
                            cell1.dishimage.image = image
                        }
                    }
            
            
            cell1.dishname1.text = dictObj["product_name"] as? String
            
            var conprice = String()
            let rupee = "$"
            if let pricedata = dictObj["price"] as? String {
                 conprice = pricedata
            }else if let pricedata = dictObj["price"] as? NSNumber {
                
                 conprice = pricedata.stringValue
            }
        
            cell1.dishprice.text = rupee + conprice
            
            cell1.addBtn.tag = indexPath.row
            cell1.addBtn.addTarget(self, action: #selector(addBtnCLicked(_:)), for: .touchUpInside)
            
//            if selectiontagArray[indexPath.item] == "no"{
//                cell1.selectedimage.isHidden = true
//                cell1.addBtn.setTitle("ADD", for: .normal)
//            }else{
//                cell1.selectedimage.isHidden = false
//                cell1.addBtn.setTitle("REMOVE", for: .normal)
//            }
            
            cell1.selectedimage.isHidden = true
            
            return cell1
            
        }
        
        
        
    }
    
    @IBAction func addBtnCLicked(_ sender: UIButton) {

        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
           
            let alert = UIAlertController(title:nil, message: "Please login to continue. We can't let just anyone have this access to ordering!",         preferredStyle: UIAlertController.Style.alert)

            alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { _ in
               
            }))
            alert.addAction(UIAlertAction(title: "SIGN IN",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            let home = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                            self.navigationController?.pushViewController(home, animated: true)
                                            
            }))
            
            self.present(alert, animated: true, completion: nil)
            alert.view.tintColor = UIColor.black
            
        }else{
        
            if restStatusStr == "open" {
        let dictObj = self.bestsellerList[sender.tag] as! NSDictionary
        
        passproductid = dictObj["product_id"] as! Int
       quantityNumber = 1
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")

        addToCart()
                
            }else{
                
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == specialoffersCollectionView {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "OfferPage") as! OfferPage
        self.navigationController?.pushViewController(home, animated: true)
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {


            let cellSize = CGSize(width: 183, height: 100)
            return cellSize

       

    }
    
}


// MARK: - Api LIst
extension HomePage {
    
    //MARK: Admin Login
    
    func adminlogincheck(){

        let urlString = GlobalClass.DevlopmentApi + "rest-auth/login/v1/"

        AF.request(urlString, method: .post, parameters: ["username":GlobalClass.adminusername, "password":GlobalClass.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                            
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(tok, forKey: "adminToken")
                            
                            self.gettiming()
                           // self.GetMenuList()
                          
                           }else{
                            
                            if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()
                               // self.sessionAlert()
                                
                            }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                
                               //  print(dict.value(forKey: "msg") as! String)
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                          
                           }
                           
                           break
                       case .failure(let error):
                        
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        
                        if error.localizedDescription == msg {
                            
                            self.showSimpleAlert(messagess:"No internet connection")
                            
                        }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                            
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                           print(error)
                       }
       }


    }
    
    //MARK: check cart Section
       
        func checkCartAvl()  {
           
           let defaults = UserDefaults.standard
           
           let savedUserData = defaults.object(forKey: "custToken")as? String
           
           let customerid = defaults.integer(forKey: "custId")
           let custidStr = String(customerid)
           
           let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
               
            let urlString = GlobalClass.DevlopmentApi+"cart/?customer_id="+custidStr+"&restaurant=" + GlobalClass.restaurantGlobalid + ""
           print("Url cust avl - \(urlString)")
               
               let headers: HTTPHeaders = [
                   "Content-Type": "application/json",
                   "Authorization": token,
                   "user_id": custidStr,
                   "action": "cart"
               ]

                AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
               response in
                 switch response.result {
                               case .success:
                                   
                                 //  print(response)

                                   if response.response?.statusCode == 200{
                                     //  self.dissmiss()
                                       
                                       let dict :NSDictionary = response.value! as! NSDictionary
                                                    //  print(dict)
                                       
                                       let status = dict.value(forKey: "results")as! NSArray
                                                      print(status)
                                                        
                                           print("store available or not - \(status)")
                                       
                                       
                                                        
                   if status.count == 0 {
                       
                  //  ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                       self.createCart()
                                                           
                   }else{
                            
                               let firstobj:NSDictionary  = status.object(at: 0) as! NSDictionary

                               let getAvbcartId = firstobj["id"] as! Int
                        let storeWRTCart = firstobj["restaurant"] as! Int


                        let defaults = UserDefaults.standard

                               defaults.set(getAvbcartId, forKey: "AvlbCartId")
                               defaults.set(storeWRTCart, forKey: "storeIdWRTCart")


                                    print("avl cart id - \(getAvbcartId)")
                                    print("cart w R to store  - \(storeWRTCart)")


                    self.cartcountApi()
                       
                 //   ERProgressHud.sharedInstance.hide()

               }
                     
               }else{
                                       
                  // self.dissmiss()
                   print(response)
                   if response.response?.statusCode == 401{
                                          
                       self.SessionAlert()
                                           
                    }else if response.response?.statusCode == 500{
                                                                      
                 //  self.dissmiss()
                                                                      
                                                                       let dict :NSDictionary = response.value! as! NSDictionary
                                                                      
                                                                      self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                                  }else{
                                                                   
                                                                   self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                                  }
               }
                                   
                                   break
                               case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {

                            self.showSimpleAlert(messagess:"No internet connection")

                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                            self.showSimpleAlert(messagess:"Slow Internet Detected")

                                }else{
                                
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    
                                }

                                   print(error)
                            }
               }
               
         
               
           }
       
       
    
    func createCart() {
        
        
        let defaults = UserDefaults.standard
        
        let savedUserData = defaults.object(forKey: "custToken")as? String
         let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
       
        let customerid = defaults.integer(forKey: "custId")
        let custidStr = String(customerid)
       

//        let urlString = GlobalObjects.DevlopmentApi+"cart/?customer_id=\(customerid)&restaurant_id=\(passedrestaurantid)"

        let urlString = GlobalClass.DevlopmentApi+"cart/"
        
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": token,
            "user_id": custidStr,
            "action": "cart"
        ]
        
        AF.request(urlString, method: .post, parameters: ["customer_id":customerid,"restaurant":GlobalClass.restaurantGlobalid],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 201{
                                                 //   self.dissmiss()
                                                    
                                                    let resultarr :NSDictionary = response.value! as! NSDictionary
                                
                                                print(resultarr)
                                
                                
                             //   let resultarr : NSArray = dict.value(forKey: "results") as! NSArray
                                
                                if resultarr.count == 0 {
                                  
                                    self.showSimpleAlert(messagess: "Cart not created")
                                    
                                }else{
                                
                             //   let dictObj = resultarr[0] as! NSDictionary

                                                    
                                                    let getAvbcartId = resultarr["id"] as! Int
                                                                                                      let storeWRTCart = resultarr["restaurant"] as! Int
                                                                                                      
                                                                                                      let defaults = UserDefaults.standard
                                                                                                      
                                                                                                      defaults.set(getAvbcartId, forKey: "AvlbCartId")
                                                                                                      defaults.set(storeWRTCart, forKey: "storeIdWRTCart")
                                                                                                           
                                                                                                           print("avl cart id - \(getAvbcartId)")
                                                                                                           print("cart w R to store  - \(storeWRTCart)")
                                                  
                                    self.cartcountApi()
                                    
                            //    self.dissmiss()
                                  
                       //     self.performSegue(withIdentifier: "productListVC", sender: self)
                                    
                                }
                                
                            //    ERProgressHud.sharedInstance.hide()

                                
                            }else{
                             
                              if response.response?.statusCode == 500{
                                                            
                                   //                         self.dissmiss()
                                                            
                                                             let dict :NSDictionary = response.value! as! NSDictionary
                                                       
                                let message  = dict["message"]as! String
                           let substr = "duplicate key value violates unique constraint"
                                
                                if message.contains(substr) {
                                    
                                    print("I found: \(message)")
                                }else{
                                    
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    
                                }
                                                        }else{
                                                            
                                                      //      self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                           }
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()

                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow Internet Detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }


     }
    
  
    func cartcountApi()
   {

      let defaults = UserDefaults.standard
      let customerid = defaults.integer(forKey: "custId")

        //  let customerid = 16
        
    let urlString = GlobalClass.DevlopmentGraphql


 //   let stringempty = "query{cartItemCount(token:\"\(GlobalObjects.globGraphQlToken)\", customerId :\(customerid)){\n count\n }\n}"

        let restidd = GlobalClass.restaurantGlobalid
       
        
        let stringempty = "query{cartItemCount(token:\"\(GlobalClass.globGraphQlToken)\",customerId :\(customerid),restaurantId:\(restidd)){\n count\n }\n}"
        
            AF.request(urlString, method: .post, parameters: ["query": stringempty],encoding: JSONEncoding.default, headers: nil).responseJSON {
           response in
             switch response.result {
                           case .success:
                              // print(response)

                               if response.response?.statusCode == 200{

                              let dict :NSDictionary = response.value! as! NSDictionary
                             // print(dict)
                                
                                if dict.count == 2 {

                                   // self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                    self.cartcountLbl.text = "0"
                                    
                                    let countStr = self.cartcountLbl.text
                                    let defaults = UserDefaults.standard
                                    defaults.set(countStr, forKey: "cartdatacount")
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    
                                }else{
                                
                                
                                
                                let status = dict.value(forKey: "data")as! NSDictionary
                              print(status)


                                let newdict = status.value(forKey: "cartItemCount")as! NSDictionary

                                let num = newdict["count"] as! Int

                                self.cartcountLbl.text = String(num)

                                    let countStr = self.cartcountLbl.text
                                    let defaults = UserDefaults.standard
                                    defaults.set(countStr, forKey: "cartdatacount")
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    
                                }
                              //  self.dissmiss()

                               }else{

                                
                                if response.response?.statusCode == 500{
                                    
                              //      ERProgressHud.sharedInstance.hide()

                              //      let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                 //   self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }else if response.response?.statusCode == 401{
                                    ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                    
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                   }
                                


                               }

                               break
                           case .failure(let error):
                            
                            ERProgressHud.sharedInstance.hide()

                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow internet detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
           }


        }
    
    
    
    
    //MARK: Webservice Call for BestSeller
        
        
        func GetMenuList(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
            }else{
                admintoken = (defaults.object(forKey: "custToken")as? String)!
            }
            let autho = "token \(admintoken)"
            var urlString = String()

            urlString = GlobalClass.DevlopmentApi+"catalog/?restaurant_id=\(GlobalClass.restaurantGlobalid)&category=Bestseller&status=ACTIVE"
                
            
            print(" category clicked menu api - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                   // print(response)

                                    if response.response?.statusCode == 200{
                                       
                                     let dict :NSDictionary = response.value! as! NSDictionary
                                     
                                        let list:NSArray = dict.value(forKey: "results") as! NSArray

                                        
                                        print(dict)
                                        
                                        
                                        if list.count == 0 {
                                           
                                    
                                            self.bestsellerList = []
                                            self.sellerCollectionView.reloadData()

                                        //    ERProgressHud.sharedInstance.hide()
                                         
                                        self.showSimpleAlert(messagess: "No menu available in this category")
                                            self.GetOfferList()
                                            
                                        }else{
                                            
                                            let settag = "no"
                                            self.selectiontagArray.append(settag)
                                            
                                            self.bestsellerList = list
                                            self.sellerCollectionView.reloadData()
                                            
                                        //    ERProgressHud.sharedInstance.hide()
                                            
                                            self.GetOfferList()

                                                }

                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {
                                        
                                self.showSimpleAlert(messagess:"No internet connection")
                                        
                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                        
                                self.showSimpleAlert(messagess:"Slow internet detected")
                                        
                                    }else{
                                    
                                        self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                }
                }
                
          
                
            }
    
    
    //MARK: Webservice Call for Offers
        
        
        func GetOfferList(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
            }else{
                admintoken = (defaults.object(forKey: "custToken")as? String)!
            }
            let autho = "token \(admintoken)"
            var urlString = String()

            urlString = GlobalClass.DevlopmentApi+"catalog/?restaurant_id=\(GlobalClass.restaurantGlobalid)&category=Offers&status=ACTIVE"
                
            
            print(" category clicked menu api - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                   // print(response)

                                    if response.response?.statusCode == 200{
                                       
                                     let dict :NSDictionary = response.value! as! NSDictionary
                                     
                                        let list:NSArray = dict.value(forKey: "results") as! NSArray

                                        
                                        print(dict)
                                        
                                        
                                        if list.count == 0 {
                                           
                                    
                                            self.OffersList = []
                                            self.specialoffersCollectionView.reloadData()

                                            ERProgressHud.sharedInstance.hide()
                                         
                                        self.showSimpleAlert(messagess: "No menu available in this category")
                                            
                                            
                                        }else{
                                            
                                            self.OffersList = list
                                            self.specialoffersCollectionView.reloadData()
                                            
                                            ERProgressHud.sharedInstance.hide()

                                                }

                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {
                                        
                                self.showSimpleAlert(messagess:"No internet connection")
                                        
                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                        
                                self.showSimpleAlert(messagess:"Slow internet detected")
                                        
                                    }else{
                                    
                                        self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                }
                }
                
          
                
            }
    
    
    //MARK: Webservice Call for add to cart

     
    func addToCart() {
       
        print(passproductid)
        
        let defaults = UserDefaults.standard
        
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        if defaults.object(forKey: "AvlbCartId") == nil {
          
            self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
           
            
        }else{
        
         let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
            let cartidStr = String(avlCartId)
            
       // let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let admintoken = defaults.object(forKey: "custToken")as? String
            
            print("customer token -\(String(describing: admintoken))")
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

        let urlString = GlobalClass.DevlopmentApi+"cart-item/"
        
            let now = Date()

                let formatter = DateFormatter()

                formatter.timeZone = TimeZone.current

                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS" ///2020-10-19 12:05:10.625

                let dateString = formatter.string(from: now)
            
            print(dateString)
            
            let commentStr = ""
            
           print("commentStr - \(commentStr)")
            
            let cartdatacount = defaults.object(forKey: "cartdatacount")as! String
            
            let metadataDict = ["updated_at":dateString,"extra":commentStr, "quantity":quantityNumber as Int, "cart_id":avlCartId as Any,"product_id":passproductid as Any,"ingredient_id":0,"sequence_id":cartdatacount]
          
            let productarr = [metadataDict]
            print("product element Dict - \(productarr)")
            
            var ingredientarray = Array<Any>()
            ingredientarray.insert(metadataDict, at: 0)
            print("Array product with ingredient added - \(ingredientarray)")

            
            let fileUrl = URL(string: urlString)

            var request = URLRequest(url: fileUrl!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(autho, forHTTPHeaderField: "Authorization")
            request.setValue(customeridStr, forHTTPHeaderField: "user_id")
            request.setValue(cartidStr, forHTTPHeaderField: "cart_id")
            request.setValue("cart-item", forHTTPHeaderField: "action")


            request.httpBody = try! JSONSerialization.data(withJSONObject: ingredientarray)

            AF.request(request)
                .responseJSON { response in
                    // do whatever you want here
                    switch response.result {
                                            case .success:
                                                print(response)
                    
                                                if response.response?.statusCode == 201{
                    
                                                    ERProgressHud.sharedInstance.hide()
                    
                                                 let alert = UIAlertController(title: nil, message: "Product added successfully into the cart",         preferredStyle: UIAlertController.Style.alert)
                    
                    
                                                   alert.addAction(UIAlertAction(title: "OK",
                                                                                 style: UIAlertAction.Style.default,
                                                                                 handler: {(_: UIAlertAction!) in
                    
                                                                                    self.cartcountApi()
                    
                                                   }))
                                                    
                                                    
                                                   self.present(alert, animated: true, completion: nil)
                                                    alert.view.tintColor = UIColor.black
                    
                                                }else{
                    
                                                    if response.response?.statusCode == 401{
                    
                                                        ERProgressHud.sharedInstance.hide()
                                                        self.SessionAlert()
                    
                                                    }else if response.response?.statusCode == 500{
                    
                                                        ERProgressHud.sharedInstance.hide()
                    
                                                        let dict :NSDictionary = response.value! as! NSDictionary
                    
                                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                    }else{
                    
                                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                       }
                    
                                                }
                    
                                                break
                                            case .failure(let error):
                                                ERProgressHud.sharedInstance.hide()
                    
                                                print(error.localizedDescription)
                    
                                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                    
                                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                    
                                                let msgrs = "URLSessionTask failed with error: The request timed out."
                    
                                                if error.localizedDescription == msg {
                    
                                            self.showSimpleAlert(messagess:"No internet connection")
                    
                                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                    
                                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                    
                                                }else{
                    
                                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                                }
                    
                                                   print(error)
                                            }
            }
            
            
        }

}
    
    //MARK: Webservice Call timing
        
        
        func gettiming(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
            }else{
                admintoken = (defaults.object(forKey: "custToken")as? String)!
            }
            
            let autho = "token \(admintoken)"
            
            let urlString = GlobalClass.DevlopmentApi+"hour/?restaurant_id=\(GlobalClass.restaurantGlobalid)"
            
               
            print(" categoryurl - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                    print(response)

                                    if response.response?.statusCode == 200{
                                        
                                        let dict :NSDictionary = response.value! as! NSDictionary
                                       
                                        let data  = dict["status"]as! String
                                        
                                        self.restStatusStr = data
                                        
                                      //  self.restStatusStr = "open"
                                        
                                       // data = "closed"
                                        
                                        if data == "closed" {
                                         
                                            print("restaurant is - \(data)")
                                            
                                            self.opcloseFlag.text = "Closed"
//
//                                            self.menulistView.alpha = 0.5
//
//                                            self.categoryview.alpha = 0.5
                                            
                                        }else{
                                            
                                            self.opcloseFlag.text = "Open"
                                            
                                            print("restaurant is - open")
                                        }
                                        
                                        self.GetMenuList()
                                        
                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {

                                self.showSimpleAlert(messagess:"No internet connection")

                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                                self.showSimpleAlert(messagess:"Slow Internet Detected")

                                    }else{
                                    
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                    
                                    
                                    
                                }
                }
                
          
                
            }
    
    
    }

// MARK: - AlertController
extension HomePage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}
    
    
