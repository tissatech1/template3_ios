//
//  CartCell.swift
//  Template3
//
//  Created by TISSA Technology on 2/19/21.
//

import UIKit

class CartCell: UITableViewCell {

    @IBOutlet weak var oterview: UIView!
    @IBOutlet weak var dishimage: UIImageView!
    @IBOutlet weak var dishname: UILabel!
    @IBOutlet weak var dishprice: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var QtyLbl: UILabel!
    @IBOutlet weak var ingredientTitL: UILabel!
    @IBOutlet weak var IngDataLbl: UITextView!
    @IBOutlet weak var boxheight: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
