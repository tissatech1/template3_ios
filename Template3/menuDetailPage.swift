//
//  menuDetailPage.swift
//  Template3
//
//  Created by TISSA Technology on 2/18/21.
//

import UIKit
import SideMenu
import SDWebImage
import Alamofire

class menuDetailPage: UIViewController,UIStepperControllerDelegate {
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var detailScrollView: UIScrollView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dishimages: UIImageView!
    @IBOutlet weak var unitpriceLbl: UILabel!
    @IBOutlet weak var customise: UILabel!
    @IBOutlet var view1: UIView!
    @IBOutlet var view2: UIView!
    @IBOutlet weak var foodtagview1: UIView!
    @IBOutlet weak var foodtagview2: UIView!
    @IBOutlet weak var foodtagview3: UIView!
    @IBOutlet var stepperController02: UIStepperController!
    @IBOutlet weak var addtocartBtn: UIButton!
    @IBOutlet weak var ingredientCollection: UICollectionView!
    @IBOutlet weak var dishnameshowLbl: UILabel!
    @IBOutlet weak var dishdiscriptionText: UITextView!
    @IBOutlet weak var pricewithcounterLbl: UILabel!
    @IBOutlet weak var bottompriceLbl: UILabel!

    
    var dishimageStrpassed = String()
    var dishnamePassed = String()
    var dishdiscriptionpassed = String()
    var passproductid = Int()
    var passunitprice = String()
    
    var quantityNumber = String()
    var productunitprice = Double()
    var IngredientConvertedPrice: [Double] = []
    var selectiontagArray: [String] = []
    var ingredientsQtyArr: [Int] = []
    var ingrediantArray = NSArray()
    var counts = [Int](repeating: 0, count: 10)
    
    let headerViewMaxHeight: CGFloat = 150
    let headerViewMinHeight: CGFloat = 70
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = UserDefaults.standard
        let restid = defaults.integer(forKey: "clickedStoreId")
        GlobalClass.restaurantGlobalid = String(restid)
        
        customise.isHidden = true
        view1.layer.cornerRadius = 10
        view1.layer.shadowColor = UIColor.lightGray.cgColor
        view1.layer.shadowOpacity = 1
        view1.layer.shadowOffset = .zero
        view1.layer.shadowRadius = 3
        
        view2.layer.cornerRadius = 10
        view2.layer.shadowColor = UIColor.lightGray.cgColor
        view2.layer.shadowOpacity = 1
        view2.layer.shadowOffset = .zero
        view2.layer.shadowRadius = 3
        
        foodtagview3.layer.cornerRadius = 5
        foodtagview2.layer.cornerRadius = 5
        foodtagview1.layer.cornerRadius = 5
        
        self.stepperController02.delegate = self // Assign instance
        
        self.stepperController02.isFloat = false
        self.stepperController02.borderColor(color: .blue)
        self.stepperController02.incrementBy(number: 1)
        self.stepperController02.count = 1
        self.stepperController02.textColor(color: .black)

        let viewNib2 = UINib(nibName: "ingredientCell", bundle: nil)
        ingredientCollection.register(viewNib2, forCellWithReuseIdentifier: "cell")
        
        addtocartBtn.layer.cornerRadius = 8
        addtocartBtn.layer.borderWidth = 1
        addtocartBtn.layer.borderColor = UIColor.white.cgColor
        
        let url = URL(string: dishimageStrpassed )
         
        dishimages.sd_imageIndicator = SDWebImageActivityIndicator.gray
        dishimages.sd_setImage(with: url) { (image, error, cache, urls) in
              if (error != nil) {
                  // Failed to load image
                self.dishimages.image = UIImage(named: "noimage.png")
              } else {
                  // Successful in loading image
                self.dishimages.image = image
              }
          }
      //  dishimages.image = UIImage(named:dishimageStrpassed)
        dishnameshowLbl.text = dishnamePassed
        dishdiscriptionText.text = dishdiscriptionpassed
        let rupee = "$"
        pricewithcounterLbl.text = rupee + passunitprice
        unitpriceLbl.text = rupee + passunitprice

        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        GetIngredientList()
        
        productunitprice = Double(passunitprice)!
        quantityNumber = "1"
    }
   

    @IBAction func backBtnClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
  
    }
    
    @IBAction func addtocartBtnClicked(_ sender: Any) {
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
           
            let alert = UIAlertController(title:nil, message: "Please login to continue. We can't let just anyone have this access to ordering!",         preferredStyle: UIAlertController.Style.alert)

            alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { _ in
               
            }))
            alert.addAction(UIAlertAction(title: "SIGN IN",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            let home = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                            self.navigationController?.pushViewController(home, animated: true)
                                            
            }))
            
            self.present(alert, animated: true, completion: nil)
            alert.view.tintColor = UIColor.black
            
        }else{
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        addToCart()
            
        }
      
    }
    

    // MARK: UIStepperControllerDelegate functions

    func stepperDidAddValues(stepper: UIStepperController) {
  
//        let countprice = stepper.count * 20.00
//        let rupee = "$"
//        pricewithcounterLbl.text = rupee + String(format: "%.2f", countprice)
        
        print("Stepper 1: \(stepper.count)")
        let qut = Int(stepper.count)
        quantityNumber = String(qut)
      //  let first = Double(passunitprice)
        
        let first = Double(passunitprice)
        let second = Double(stepper.count)
        
        var sum = Double()
        sum = first! * second
        
        productunitprice = sum
        
        let totalQty = IngredientConvertedPrice.reduce(0, +)
        
        let withingsum = totalQty + sum
        
            let rupee = "$"
            
        pricewithcounterLbl.text = rupee + String(format: "%.2f", withingsum)
        bottompriceLbl.text = rupee + String(format: "%.2f", withingsum)
    }
    
    func stepperDidSubtractValues(stepper: UIStepperController) {
  
//        let countprice = stepper.count * 20.00
//        let rupee = "$"
//        pricewithcounterLbl.text = rupee + String(format: "%.2f", countprice)
        
        
        print("Stepper 1: \(stepper.count)")
        let qut = Int(stepper.count)
        quantityNumber = String(qut)
      //  let first = Double(passunitprice)
        
        let first = Double(passunitprice)
        let second = Double(stepper.count)
        
        var sum = Double()
        sum = first! * second
        
        productunitprice = sum
        
        let totalQty = IngredientConvertedPrice.reduce(0, +)
        
        let withingsum = totalQty + sum
        
            let rupee = "$"
            
        pricewithcounterLbl.text = rupee + String(format: "%.2f", withingsum)
        bottompriceLbl.text = rupee + String(format: "%.2f", withingsum)

        
    }

    
}

// MARK: - UIScrollViewDelegate
extension menuDetailPage: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y: CGFloat = scrollView.contentOffset.y
        let newHeaderViewHeight: CGFloat = headerViewHeightConstraint.constant - y

        if newHeaderViewHeight > headerViewMaxHeight {
            headerViewHeightConstraint.constant = headerViewMaxHeight
            dishimages.isHidden = false
            unitpriceLbl.isHidden = false
            customise.isHidden = true

        } else if newHeaderViewHeight < headerViewMinHeight {
            headerViewHeightConstraint.constant = headerViewMinHeight
            dishimages.isHidden = true
            unitpriceLbl.isHidden = true
            customise.isHidden = false

        } else {
            headerViewHeightConstraint.constant = newHeaderViewHeight
            scrollView.contentOffset.y = 0 // block scroll view
        }
    }
}
// MARK: - UICollectionViewDataSource
extension menuDetailPage: UICollectionViewDataSource,UICollectionViewDelegate {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return ingrediantArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
       
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ingredientCell
            
        
        let dictObj = self.ingrediantArray[indexPath.row] as! NSDictionary
        
        cell1.ingredientName.text!  = dictObj["ingredient_name"] as! String
        let rupee = "$ "
        
            let pricedata = dictObj["price"]as! String
            let conprice = String(pricedata)
        
        cell1.ingredientPrice.text = rupee + conprice
        
        var urlStr = String()
        if dictObj["ingredient_url"] is NSNull || dictObj["ingredient_url"] == nil{

                    urlStr = ""

                }else{
                    urlStr = dictObj["ingredient_url"] as! String
                }

                let url = URL(string: urlStr )


        cell1.ingredientImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell1.ingredientImage.sd_setImage(with: url) { (image, error, cache, urls) in
                    if (error != nil) {
                        // Failed to load image
                        cell1.ingredientImage.image = UIImage(named: "noimage.png")
                    } else {
                        // Successful in loading image
                        cell1.ingredientImage.image = image
                    }
                }
        
         
        cell1.backview.layer.borderColor = UIColor.lightGray.cgColor
        cell1.backview.layer.borderWidth = 0.5
        cell1.backview.layer.shadowOpacity = 1
        cell1.backview.layer.shadowOffset = .zero
        cell1.backview.layer.shadowRadius = 3
        
        if selectiontagArray[indexPath.item] == "NO"{
            cell1.selectedview.isHidden = true
        }else{
            cell1.selectedview.isHidden = false
        }
        
            return cell1
        
    }
    

    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        

      //  let cell = ingredientCollection.cellForItem(at: indexPath)as! ingredientCell
        
        if selectiontagArray[indexPath.item] == "NO"{
            
            let cell = ingredientCollection.cellForItem(at: indexPath)as! ingredientCell
            cell.selectedview.isHidden = false


            let dictObj = self.ingrediantArray[indexPath.item] as! NSDictionary
            let pricedata = dictObj["price"]as! String

            IngredientConvertedPrice[indexPath.item] = Double(pricedata)!
            
            let totalQty = IngredientConvertedPrice.reduce(0, +)

            print("product unit price = \(productunitprice)")

            let addedprice = productunitprice + totalQty


            let rupee = "$"

            pricewithcounterLbl.text = rupee + String(format: "%.2f", addedprice)
            bottompriceLbl.text = rupee + String(format: "%.2f", addedprice)
           
            selectiontagArray[indexPath.row] = "YES"

            print("selectatgarray - \(selectiontagArray)")
            
            ingredientsQtyArr[indexPath.row] = 1
            counts[indexPath.row] += 1
            
        }else{
            
            let cell = ingredientCollection.cellForItem(at: indexPath)as! ingredientCell
            cell.selectedview.isHidden = true

            
            let dictObj = self.ingrediantArray[indexPath.row] as! NSDictionary
            let pricedata = dictObj["price"]as! String

            counts[indexPath.row] = 0
            IngredientConvertedPrice[indexPath.row] = 0.00
            ingredientsQtyArr[indexPath.row] = 0
            
            let totalQty = IngredientConvertedPrice.reduce(0, +)

            print("product unit price = \(productunitprice)")

            let addedprice = productunitprice + totalQty

            let rupee = "$"

            pricewithcounterLbl.text = rupee + String(format: "%.2f", addedprice)
            bottompriceLbl.text = rupee + String(format: "%.2f", addedprice)
           
            selectiontagArray[indexPath.row] = "NO"
            print("selectatgarray - \(selectiontagArray)")

            cell.ingredientPrice.text = rupee + pricedata
            
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {


            let cellSize = CGSize(width: 183, height: 100)
            return cellSize

       

    }
    
}


// MARK: - Api
extension menuDetailPage{
    
    func GetIngredientList(){
        
        var admintoken = String()
        let defaults = UserDefaults.standard
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            admintoken = (defaults.object(forKey: "adminToken")as? String)!
        }else{
            admintoken = (defaults.object(forKey: "custToken")as? String)!
        }
        
        let autho = "token \(admintoken)"
        let urlString = GlobalClass.DevlopmentApi+"ingredient/?product_id=\(passproductid)&status=ACTIVE"
        
           
        print(" categoryurl - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]
      

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                 
                                    let dict :NSDictionary = response.value! as! NSDictionary

                                    let ingredientsList:NSArray = dict.value(forKey: "results")as! NSArray

                                    
                                    if ingredientsList.count == 0 {
                                    
                                        self.ingredientCollection.isHidden = true
                                        self.ingrediantArray = []
                                        self.ingredientCollection.reloadData()
                                        
                                    }else{
                                        
                                        for index in ingredientsList {
                                            
                                            let settag = "NO"
                                            let qty = 0
                                            let ingPrice = 0.00
                                            self.selectiontagArray.append(settag)
                                            self.ingredientsQtyArr.append(qty)
                                            self.IngredientConvertedPrice.append(ingPrice)
                                        }
                                        
                                        print("selectiontag - \(self.selectiontagArray)")
                                        
                                        self.ingredientCollection.isHidden = false
                                        
                                        
                                        self.ingrediantArray = ingredientsList
                                        self.ingredientCollection.reloadData()
                                        
                                    }
                                    
                                    
                                    
                                    print("ingredientsList = \(ingredientsList)")
                                    
                                    ERProgressHud.sharedInstance.hide()

                                 
                                }else{
                                    
                  if response.response?.statusCode == 401{
                                    
                    ERProgressHud.sharedInstance.hide()

                    self.SessionAlert()
                          
                                    
                                    }
                                    
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    //MARK: Webservice Call for add to cart

     
    func addToCart() {
        
        
        print(passproductid)
        
        let defaults = UserDefaults.standard
        
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        if defaults.object(forKey: "AvlbCartId") == nil {
          
            self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
           
            
        }else{
        
         let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
            let cartidStr = String(avlCartId)
            
       // let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let admintoken = defaults.object(forKey: "custToken")as? String
            
            print("customer token -\(String(describing: admintoken))")
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

        let urlString = GlobalClass.DevlopmentApi+"cart-item/"
        
//        let headers: HTTPHeaders = [
//            "Content-Type": "application/json",
//            "Authorization": autho,
//            "user_id": customeridStr,
//            "cart_id": cartidStr,
//            "action": "cart-item"
//        ]

            let now = Date()

                let formatter = DateFormatter()

                formatter.timeZone = TimeZone.current

                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS" ///2020-10-19 12:05:10.625

                let dateString = formatter.string(from: now)
            
            print(dateString)
            let commentStr = ""
            
//            if textbaseViewTf.text == nil || textbaseViewTf.text! == "" {
//                commentStr = ""
//            }else{
//
//                commentStr = textbaseViewTf.text! as String
//            }
            
       //    print("commentStr - \(commentStr)")
            
            let cartdatacount = defaults.object(forKey: "cartdatacount")as! String
           // let sendcartcount = cartdatacount
            
            let qtyy = Int(quantityNumber)
            
            let metadataDict = ["updated_at":dateString,"extra":commentStr, "quantity":qtyy! as Int, "cart_id":avlCartId as Any,"product_id":passproductid as Any,"ingredient_id":0,"sequence_id":cartdatacount]
          
            let productarr = [metadataDict]
            print("product element Dict - \(productarr)")
            
            var ingredientarray = Array<Any>()
            
            for (index, selectedIn) in selectiontagArray.enumerated() {
                
                let dictObj = self.ingrediantArray[index] as! NSDictionary
                let ingredientidd = dictObj["ingredient_id"]as! NSNumber
                let idddStr = ingredientidd.stringValue
                let checkingstr = selectedIn
                
                let individualingredientQty = ingredientsQtyArr[index]
                
                if checkingstr == "YES" {
                    
                    let ingredientDict = ["updated_at":dateString,"extra":"test", "quantity":individualingredientQty, "cart_id":avlCartId as Any,"product_id":passproductid as Any,            "ingredient_id":idddStr,"sequence_id":cartdatacount]
                    
                    ingredientarray.append(ingredientDict)
                    
                }
                
            }
        
            
            ingredientarray.insert(metadataDict, at: 0)
            print("Array product with ingredient added - \(ingredientarray)")

        
            let fileUrl = URL(string: urlString)

            var request = URLRequest(url: fileUrl!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(autho, forHTTPHeaderField: "Authorization")
            request.setValue(customeridStr, forHTTPHeaderField: "user_id")
            request.setValue(cartidStr, forHTTPHeaderField: "cart_id")
            request.setValue("cart-item", forHTTPHeaderField: "action")


            request.httpBody = try! JSONSerialization.data(withJSONObject: ingredientarray)

            AF.request(request)
                .responseJSON { response in
                    // do whatever you want here
                    switch response.result {
                                            case .success:
                                                print(response)
                    
                                                if response.response?.statusCode == 201{
                    
                                                    ERProgressHud.sharedInstance.hide()
                    
                                                 let alert = UIAlertController(title: nil, message: "Product added successfully into the cart",         preferredStyle: UIAlertController.Style.alert)
                    
                    
                                                   alert.addAction(UIAlertAction(title: "OK",
                                                                                 style: UIAlertAction.Style.default,
                                                                                 handler: {(_: UIAlertAction!) in
                    
                                                                                    let order = self.storyboard?.instantiateViewController(withIdentifier: "ExploremenuPage") as! ExploremenuPage
                                                                                    self.navigationController?.pushViewController(order, animated: false)
                                                                                    
                    
                                                   }))
                                                   self.present(alert, animated: true, completion: nil)
                                                    alert.view.tintColor = UIColor.black
                    
                                                }else{
                    
                                                    if response.response?.statusCode == 401{
                    
                                                        ERProgressHud.sharedInstance.hide()
                                                        self.SessionAlert()
                    
                                                    }else if response.response?.statusCode == 500{
                    
                                                        ERProgressHud.sharedInstance.hide()
                    
                                                        let dict :NSDictionary = response.value! as! NSDictionary
                    
                                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                    }else{
                    
                                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                       }
                    
                                                }
                    
                                                break
                                            case .failure(let error):
                                                ERProgressHud.sharedInstance.hide()
                    
                                                print(error.localizedDescription)
                    
                                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                    
                                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                    
                                                let msgrs = "URLSessionTask failed with error: The request timed out."
                    
                                                if error.localizedDescription == msg {
                    
                                            self.showSimpleAlert(messagess:"No internet connection")
                    
                                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                    
                                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                    
                                                }else{
                    
                                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                                }
                    
                                                   print(error)
                                            }
            }
            
            
        }

    }
}

// MARK: - AlertController
extension menuDetailPage{
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: messagess, message: nil,         preferredStyle: UIAlertController.Style.alert)

      //  alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action//
      //  }))
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
        }))
        
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                       
                                        let LoginPage = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(LoginPage, animated: true)
                                        
                                        
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    

}
