//
//  detailproductCell.swift
//  Template3
//
//  Created by TISSA Technology on 2/24/21.
//

import UIKit

class detailproductCell: UITableViewCell {

    @IBOutlet weak var productimage: UIImageView!
    @IBOutlet weak var productname: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var priceLblshow: UILabel!
    @IBOutlet weak var outerview: UIView!
    @IBOutlet weak var productdescription: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
