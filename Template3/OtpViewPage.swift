//
//  OtpViewPage.swift
//  Template3
//
//  Created by TISSA Technology on 2/25/21.
//

import UIKit
import Alamofire

class OtpViewPage: BottomPopupViewController {
    
    
    @IBOutlet weak var blurview: UIView!
    @IBOutlet weak var otpTF: UITextField!
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var mobileTF: UITextField!
    @IBOutlet weak var codeTF: UITextField!
    
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    
    var guestmobile = String()
    var verifyCode = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        blurview.isHidden = true
        otpView.layer.cornerRadius = 8
    }
    

    @IBAction func backClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func otpBackClicked(_ sender: Any) {
        
        blurview.isHidden = true
        
    }
    
    @IBAction func sendotpClicked(_ sender: Any) {
        
       
        if codeTF.text == nil || codeTF.text == "" || codeTF.text == " " {

            showSimpleAlert(messagess: "Enter country code")
        }else if mobileTF.text == nil || mobileTF.text == "" || mobileTF.text == " " {

            showSimpleAlert(messagess: "Enter mobile number")
        }else{

        guestmobile = codeTF.text!
            + mobileTF.text!

         ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        GestUserApi()

        }
        
        
    }
    
    
    @IBAction func submitClicked(_ sender: Any) {
        
        if otpTF.text == nil || otpTF.text == "" || otpTF.text == " "{
            
            showSimpleAlert(messagess: "Enter OTP")
        }else{
        
            GlobalClass.globalverifyCode = otpTF.text!
            GlobalClass.globalguestMobile = guestmobile
                    dismiss(animated: true, completion: nil)
                    GlobalClass.OtpSubmitClicked = "yes"
        }
        

    }
    
    override var popupHeight: CGFloat { return height ?? CGFloat(300) }
    
    override var popupTopCornerRadius: CGFloat { return topCornerRadius ?? CGFloat(10) }
    
    override var popupPresentDuration: Double { return presentDuration ?? 1.0 }
    
    override var popupDismissDuration: Double { return dismissDuration ?? 1.0 }
    
    override var popupShouldDismissInteractivelty: Bool { return shouldDismissInteractivelty ?? true }
    
    override var popupDimmingViewAlpha: CGFloat { return BottomPopupConstants.kDimmingViewDefaultAlphaValue }

}

// MARK: - AlertController
extension OtpViewPage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
}


// MARK: - Api
extension OtpViewPage {
    
    func GestUserApi() {


        let urlString = GlobalClass.DevlopmentApi+"send/code/"

         AF.request(urlString, method: .post, parameters: ["phone_number": guestmobile],encoding: JSONEncoding.default, headers: nil).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 200{
                       
                                ERProgressHud.sharedInstance.hide()

                                GlobalClass.OtpSubmitClicked = "no"
                                self.blurview.isHidden = false
                                
                             
                            }else{
                             
                              if response.response?.statusCode == 403{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                 
                                 self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                             }else if response.response?.statusCode == 401{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 self.showSimpleAlert(messagess: "Username or password is incorrect")
                                
                             }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                self.showSimpleAlert(messagess: "\(self.guestmobile) is not a valid phone number.")
                               
                            }else{
                                 
                                 self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                }
                             

                            }
                            
                            break
                        case .failure(let error):
                         ERProgressHud.sharedInstance.hide()
                         print(error.localizedDescription)
                         
                         let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                         
                         
                         let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                         
                         let msgrs = "URLSessionTask failed with error: The request timed out."
                         
                         if error.localizedDescription == msg {
                             
                             self.showSimpleAlert(messagess:"No Internet Detected")
                             
                         }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                             
                             self.showSimpleAlert(messagess:"Slow Internet Detected")
                             
                         }else{
                         
                             self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                         }

                            print(error)
                        }
        }


     }
     
    
}
