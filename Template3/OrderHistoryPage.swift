//
//  OrderHistoryPage.swift
//  Template3
//
//  Created by TISSA Technology on 2/24/21.
//

import UIKit
import Alamofire
import SDWebImage
import SideMenu

class OrderHistoryPage: UIViewController {
    @IBOutlet weak var ordertable: UITableView!
    var orderlistaraay = NSArray()
    var orderclickid = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        getorderlist()
        
        ordertable.register(UINib(nibName: "OrderHistoryCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        ordertable.rowHeight = UITableView.automaticDimension
        ordertable.estimatedRowHeight = 170
        
    }
    
    @IBAction func backClicked(_ sender: Any) {
        
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as! HomePage
        self.navigationController?.pushViewController(detail, animated: false)
    }

}
// MARK: - uiTableViewDatasource
extension OrderHistoryPage: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return orderlistaraay.count
    
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrderHistoryCell
        cell.selectionStyle = .none
        
       // cell.menunameLbl.text = menudishName[indexPath.row]
        
        cell.outerview.layer.cornerRadius = 5
        cell.outerview.layer.shadowColor = UIColor.lightGray.cgColor
        cell.outerview.layer.shadowOpacity = 1
        cell.outerview.layer.shadowOffset = .zero
        cell.outerview.layer.shadowRadius = 3
        
        cell.ordernumberView.layer.cornerRadius = 5
        cell.deleteBtn.layer.cornerRadius = 5
        cell.deleteBtn.layer.borderWidth = 0.5
        cell.deleteBtn.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(DeleteBtnPredded(_:)), for: .touchUpInside)
        
        let dictObj = self.orderlistaraay[indexPath.row] as! NSDictionary

        let urlStr = dictObj["status"] as! String
    
         if urlStr == "CONFIRMED" {
             
             cell.itemsLbl.text = "Status : Confirmed"

            cell.deleteBtn.isHidden = true
             
         }else if urlStr == "SUCCESS"{
             
             cell.itemsLbl.text = "Status : Success"

            cell.deleteBtn.isHidden = true
             
         }else if urlStr == "FAIL"{
             
             cell.itemsLbl.text = "Status : Failed"
    
            cell.deleteBtn.isHidden = true
             
         }else if urlStr == "READY_TO_FULFILL"{
             
             cell.itemsLbl.text = "Status : Ready to fulfill"
             cell.deleteBtn.isHidden = true
             
         }else if urlStr == "READY_TO_PICK"{
             
             cell.itemsLbl.text = "Status : Ready to pick"
             cell.deleteBtn.isHidden = true
             
         }else if urlStr == "PICKED"{
             
             cell.itemsLbl.text = "Status : Picked"
             cell.deleteBtn.isHidden = true
             
         }else if urlStr == "COMPLETE"{
             
             cell.itemsLbl.text = "Status : Completed"
             cell.deleteBtn.isHidden = true
             
         }else if urlStr == "CANCELED"{
             
             cell.itemsLbl.text = "Status : Cancelled"
             cell.deleteBtn.isHidden = true
             
         }
        
        let rupee = "for amount $"
         
        let priceLbl = dictObj["amount"]as! String

        cell.amountLbl.text =  rupee + priceLbl
        
        let strdate = (dictObj["created_at"] as! String)

        let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
                dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                let date = dateFormatter.date(from: strdate)// create   date from string

                // change to a readable time format and change to local time zone
       if date == nil {
        
           let dateFormatter = DateFormatter()
                   dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                   dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                   let date1 = dateFormatter.date(from: strdate)
           dateFormatter.dateFormat = "MMMM dd,yyyy"
           dateFormatter.timeZone = NSTimeZone.local
           let timeStamp1 = dateFormatter.string(from: date1!)
   
   let textsh = "Order Placed on "
   cell.orderplacedateLbkl.text = textsh + timeStamp1
           
       }else{
       
                dateFormatter.dateFormat = "MMMM dd,yyyy"
                dateFormatter.timeZone = NSTimeZone.local
                let timeStamp = dateFormatter.string(from: date!)
        let textsh = "Order Placed on "
        
        cell.orderplacedateLbkl.text = textsh + timeStamp
       }
        
        let storedata:NSDictionary = dictObj["order"]as! NSDictionary
        
        let orderno = storedata["order_id"] as! Int
       let strsh = "Order No "
       cell.ordernumLbl.text = strsh + String(orderno)
        
        cell.itemlistLbl.text = "Payment method : " + (dictObj["payment_method"] as! String)

        
        return cell
    }
    
    @IBAction func DeleteBtnPredded(_ sender: UIButton) {

        let index = sender.tag
       
        let dictObj = self.orderlistaraay[index] as! NSDictionary
        
        let storedata:NSDictionary = dictObj["order"]as! NSDictionary
        
        let orderno = storedata["order_id"] as! Int
        
        orderclickid = String(orderno)
        
       
        let alert = UIAlertController(title: nil, message: "Are you sure you want to cancel?",         preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action//
        }))
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        //Sign out action
                                        ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                         self.deleteorderapi()
                                         
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
        
        
        
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
            return  UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let orderdel = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailPage") as! OrderDetailPage
        
        let dictObj = self.orderlistaraay[indexPath.row] as! NSDictionary

        let rupee = "for amount $"
         
        let priceLbl = dictObj["amount"]as! String

        orderdel.amtStr  =  rupee + priceLbl
        
        let storedata:NSDictionary = dictObj["order"]as! NSDictionary
        orderdel.orderIDGet = storedata["order_id"] as! Int

        let strdate = (dictObj["created_at"] as! String)

        let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
                dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                let date = dateFormatter.date(from: strdate)// create   date from string

                // change to a readable time format and change to local time zone
       if date == nil {
        
           let dateFormatter = DateFormatter()
                   dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                   dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                   let date1 = dateFormatter.date(from: strdate)
           dateFormatter.dateFormat = "MMMM dd,yyyy"
           dateFormatter.timeZone = NSTimeZone.local
           let timeStamp1 = dateFormatter.string(from: date1!)
   
           let textsh = "Order Placed on "
           orderdel.dateStr = textsh + timeStamp1
           
       }else{
       
                dateFormatter.dateFormat = "MMMM dd,yyyy"
                dateFormatter.timeZone = NSTimeZone.local
                let timeStamp = dateFormatter.string(from: date!)
        let textsh = "Order Placed on "
        
        orderdel.dateStr = textsh + timeStamp
       }
        
        self.navigationController?.pushViewController(orderdel, animated: true)
        
    }
}

// MARK: - AlertController
extension OrderHistoryPage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}

// MARK: - Api LIst
extension OrderHistoryPage {
    
    func getorderlist()  {
        
        let defaults = UserDefaults.standard
        
      //  let admintoken = defaults.object(forKey: "adminToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        let admintoken = defaults.object(forKey: "custToken")as? String
           
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
      //  let trimmedString = categoryStr.removingAllWhitespaces()
        
        let urlString = GlobalClass.DevlopmentApi+"customer-payment/?customer_id=\(customerId)&restaurant_id=\(GlobalClass.restaurantGlobalid)"
         
        
        print("oderitems get url - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "action": "customer-payment"
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                
                                    self.orderlistaraay  = response.value! as! NSArray
                                     
                                    print( self.orderlistaraay)
                                    
                                    if self.orderlistaraay.count == 0 {
                                       
                                        ERProgressHud.sharedInstance.hide()
                                        self.showSimpleAlert(messagess:"No order available")
                                        
                                        
                                        
                                    }else{
                                        
                                        
                                        ordertable.reloadData()
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        
                                    }
                                   
                    
                                 
                                }else{
                                    
                        if response.response?.statusCode == 401{
                                    
                            ERProgressHud.sharedInstance.hide()
                        self.SessionAlert()
                          
                           }else if response.response?.statusCode == 500{
                                        
                            ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                           }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func deleteorderapi()  {
        
        let defaults = UserDefaults.standard
        
      //  let admintoken = defaults.object(forKey: "adminToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        let admintoken = defaults.object(forKey: "custToken")as? String
           
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
      //  let trimmedString = categoryStr.removingAllWhitespaces()
        
        let urlString = GlobalClass.DevlopmentApi+"payment/\(orderclickid)"
        
       

            
        print("delete get url - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "order_id": orderclickid,
                "user_id": customeridStr,
                "action": "payment"
            ]

        AF.request(urlString, method: .delete, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                
                                   
                                    ERProgressHud.sharedInstance.hide()

                                    let alert = UIAlertController(title: "Order cencel successfully", message: nil,         preferredStyle: UIAlertController.Style.alert)

                                  
                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in
                                                                    //Sign out action
                                                                    ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                                                                getorderlist()
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor.black
                                   
                    
                                 
                                }else{
                                    
                        if response.response?.statusCode == 401{
                                    
                            ERProgressHud.sharedInstance.hide()
                        self.SessionAlert()
                          
                           }else if response.response?.statusCode == 500{
                                        
                            ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                            
                           }else if response.response?.statusCode == 400{
                            
                            ERProgressHud.sharedInstance.hide()

                            let dict :NSDictionary = response.value! as! NSDictionary
                            
                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
               }else{
                                        
                      self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
}
