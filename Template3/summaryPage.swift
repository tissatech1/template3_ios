//
//  summaryPage.swift
//  Template3
//
//  Created by TISSA Technology on 2/24/21.
//

import UIKit
import Alamofire
import SDWebImage
import SideMenu

class summaryPage: UIViewController {
    @IBOutlet weak var trackview: UIView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var producttable: UITableView!
    @IBOutlet weak var tableheightconstant: NSLayoutConstraint!

    @IBOutlet weak var orderNoLbl: UILabel!
    @IBOutlet weak var addline1Lbl: UILabel!
    @IBOutlet weak var subtotalLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var discountlbl: UILabel!
    @IBOutlet weak var orderTotalLbl: UILabel!
    @IBOutlet weak var paymentMethodtitle: UILabel!

    
    var fetchedItems = NSArray()
    var dateStr = String()
    var amtStr = String()
    var orderIDGet = Int()
   
    override func viewDidLoad() {
        super.viewDidLoad()

        trackview.layer.cornerRadius = 10
        detailView.layer.shadowColor = UIColor.lightGray.cgColor
        detailView.layer.shadowOpacity = 1
        detailView.layer.shadowOffset = .zero
        detailView.layer.shadowRadius = 3
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
         orderitems()
         setdata()
        
        producttable.register(UINib(nibName: "summaryTableCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        producttable.rowHeight = UITableView.automaticDimension
        producttable.estimatedRowHeight = 44
        paymentMethodtitle.text = "Card"
    }
    
    func setdata() {
        let defaultns = UserDefaults.standard
        let order:NSDictionary = defaultns.object(forKey: "passOrderResult")as! NSDictionary

        print("orderInfo dict - \(order)")
        
        let numbersh = order["order_id"]as! Int
        
        self.orderNoLbl.text! = String(numbersh)

        let strdate = order["created_at"]as! String
        
        let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
                dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                let date = dateFormatter.date(from: strdate)// create   date from string

                // change to a readable time format and change to local time zone
                dateFormatter.dateFormat = "MMMM dd, yyyy"
                dateFormatter.timeZone = NSTimeZone.local
                let timeStamp = dateFormatter.string(from: date!)
                
        self.orderNoLbl.text! = "Order No " + String(numbersh) + "  |  " + timeStamp

        self.subtotalLbl.text! = "$\(order["subtotal"]as! String)"
        self.taxLbl.text! = "$\(order["tax"]as! String)"
        self.discountlbl.text! = "$\(order["discount"]as! String)"
        self.orderTotalLbl.text! = "$\(order["total"]as! String)"
        
                    let defaults = UserDefaults.standard
                    let billing:NSDictionary = defaults.object(forKey: "billingaddressDICT")as! NSDictionary
        
                    print("billing dict - \(billing)")
        
        let one = billing["house_number"]as! String
              let two = billing["address"]as! String
        
        let addlineLbl = billing["name"]as! String
        let addline2Lbl = two + " " + one
        let addline3Lbl = billing["city"]as! String
        let addline4Lbl = billing["state"]as! String
        let addline5Lbl = billing["country"]as! String
        let addline6Lbl = billing["zip"] as! String
                
        addline1Lbl.text = addlineLbl + ", " + addline2Lbl + ", " + addline3Lbl + ", " + addline4Lbl + ", " + addline5Lbl + ", " + addline6Lbl
        
        
    }
    
    
    @IBAction func exploreBtnClicked(_ sender: Any) {
        
        let explore = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as! HomePage
        self.navigationController?.pushViewController(explore, animated: true)
        
    }
    
    @IBAction func trackorderclicked(_ sender: Any) {
        
        let order = self.storyboard?.instantiateViewController(withIdentifier: "OrderHistoryPage") as! OrderHistoryPage
        self.navigationController?.pushViewController(order, animated: true)
        
    }
    
}
// MARK: - uiTableViewDatasource
extension summaryPage: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return fetchedItems.count
    
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! summaryTableCell
        cell.selectionStyle = .none
        
        if fetchedItems.count == 0 {
            
        }else{
        
        
         let dictObj = self.fetchedItems[indexPath.row] as! NSDictionary

        cell.menunameLbl.text = dictObj["product_name"] as? String

            let rupee = "$"

            let pricedata = dictObj["line_total"]as! String

           cell.priceLbl.text = rupee + pricedata

        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
            return  UITableView.automaticDimension
        
    }
}

// MARK: - Api
extension summaryPage {
    
    func orderitems()  {
        
        let defaults = UserDefaults.standard
        
      //  let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let admintoken = defaults.object(forKey: "custToken")as? String
           
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
      //  let trimmedString = categoryStr.removingAllWhitespaces()
        
        let urlString = GlobalClass.DevlopmentApi+"order-item/?order_id=\(orderIDGet)"
        
       

            
        print("oderitems get url - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                
                                    let dict1 :NSDictionary = response.value! as! NSDictionary
                                     
                                   self.fetchedItems = (dict1.value(forKey:"results")as! NSArray)
                                      // print(self.productList)
                                         if self.fetchedItems.count == 0 {
                                           
                                       
                                            
                                            self.producttable.reloadData()
                                            
                                            ERProgressHud.sharedInstance.hide()
                                         }else{
                                            
                                            let sun = self.fetchedItems.count
                                            
                                            
                                    self.tableheightconstant.constant = CGFloat(45 * Int(sun))
                                            
                                            self.producttable.reloadData()
                                            
                                            ERProgressHud.sharedInstance.hide()

                                         }
                    
                                 
                                }else{
                                    
                        if response.response?.statusCode == 401{
                                    
                            ERProgressHud.sharedInstance.hide()
                        self.SessionAlert()
                          
                           }else if response.response?.statusCode == 500{
                                        
                            ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                           }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
}


// MARK: - AlertController
extension summaryPage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}
