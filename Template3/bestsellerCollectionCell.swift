//
//  bestsellerCollectionCell.swift
//  Template3
//
//  Created by TISSA Technology on 2/16/21.
//

import UIKit

class bestsellerCollectionCell: UICollectionViewCell {
    @IBOutlet weak var dishimage: UIImageView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var blurview: UIView!
    @IBOutlet weak var dishname1: UILabel!
    @IBOutlet weak var dishprice: UILabel!
    @IBOutlet weak var selectedimage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
