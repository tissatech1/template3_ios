//
//  PaymentPage.swift
//  Template3
//
//  Created by TISSA Technology on 2/23/21.
//

import UIKit
import Alamofire
import SDWebImage
import SideMenu

class PaymentPage: BottomPopupViewController {
    
    @IBOutlet weak var cardholdernameTF: UITextField!
    @IBOutlet weak var creditcardnumberTF: UITextField!
    @IBOutlet weak var securityTF: UITextField!
    @IBOutlet weak var cardDTETF: UITextField!
    var MonthStr = String()
    var YearStr = String()
    
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    
    private var selectedDate: AVDate?
    private let calendar: AVCalendarViewController = AVCalendarViewController.calendar

    override func viewDidLoad() {
        super.viewDidLoad()

        GlobalClass.PayClicked = "no"
    }
    
    @IBAction func paymentClicked(_ sender: UIButton) {
        
        if cardholdernameTF.text == "" || cardholdernameTF.text == nil {
            showSimpleAlert(messagess: "Enter card holder name")
        }else if creditcardnumberTF.text == "" || creditcardnumberTF.text == nil {
            showSimpleAlert(messagess: "Enter card number")
        }else if securityTF.text == "" || securityTF.text == nil {
            showSimpleAlert(messagess: "Enter security code")
        }else if MonthStr == "" || MonthStr.isEmpty {
            showSimpleAlert(messagess: "Enter Date")
        }else if YearStr == "" || YearStr.isEmpty {
            showSimpleAlert(messagess: "Enter Date")
        }else{

            GlobalClass.cardNumber = creditcardnumberTF.text!
            GlobalClass.monthNumber = MonthStr
            GlobalClass.yearNumber = YearStr
            GlobalClass.CvvNumber = securityTF.text!
            GlobalClass.PayClicked = "yes"
            dismiss(animated: true, completion: nil)

        }
        
        
    }
    
    @IBAction func calencerBtnCLicked(_ sender: UIButton) {
        
        showTheCalendar()
        
    }
    
    private func showTheCalendar() {
        calendar.dateStyleComponents = CalendarComponentStyle(backgroundColor: UIColor(red: 89/255, green: 65/255, blue: 102/255, alpha: 1.0),
                                                              textColor: .white,
                                                              highlightColor: UIColor(red: 126/255, green: 192/255, blue: 196/255, alpha: 1.0).withAlphaComponent(0.5))
        calendar.yearStyleComponents = CalendarComponentStyle(backgroundColor: UIColor(red: 94/255, green: 200/255, blue: 252/255, alpha: 1.0),
                                                              textColor: .black, highlightColor: .white)
        calendar.monthStyleComponents = CalendarComponentStyle(backgroundColor: UIColor(red: 47/255, green: 60/255, blue: 95/255, alpha: 1.0),
                                                               textColor: UIColor(red: 126/255, green: 192/255, blue: 196/255, alpha: 1.0),
                                                               highlightColor: UIColor.white)
        calendar.subscriber = { [weak self] (date) in guard let checkedSelf = self else { return }
            if date != nil {
                checkedSelf.selectedDate = date
                let _ = Date(timeIntervalSince1970: TimeInterval(date?.doubleVal ?? 0))
              //  if let day = date?.day, let month = date?.month, let year = date?.year {
                
                if let month = date?.month, let year = date?.year {
                    let dateString = month + "/" + year
                    let datemonth = month
                    let dateyear = year
                    
                    let dateappend = month + "/" + year
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM/yyyy"
                    let enteredDate = dateFormatter.date(from: dateappend)!
                    let endOfMonth = Calendar.current.date(byAdding: .month, value: 1, to: enteredDate)!
                    let now = Date()
                    if (endOfMonth < now) {
                        
                        self!.showSimpleAlert(messagess: "Expired month")
                        
                        self!.cardDTETF.text = ""
                        self!.cardDTETF.placeholder = "MM/YYYY"
                        self!.MonthStr = ""
                        self!.YearStr = ""
                        
                    } else {
                        // valid
                        print("valid - now: \(now) entered: \(enteredDate)")
                        
                        self!.cardDTETF.text = dateString
                        if datemonth == "Jan" {
                            self!.MonthStr = "01"
                        }else if datemonth == "Feb" {
                            self!.MonthStr = "02"
                        }else if datemonth == "Mar" {
                            self!.MonthStr = "03"
                        }else if datemonth == "Apr" {
                            self!.MonthStr = "04"
                        }else if datemonth == "May" {
                            self!.MonthStr = "05"
                        }else if datemonth == "Jun" {
                            self!.MonthStr = "06"
                        }else if datemonth == "Jul" {
                            self!.MonthStr = "07"
                        }else if datemonth == "Aug" {
                            self!.MonthStr = "08"
                        }else if datemonth == "Sept" {
                            self!.MonthStr = "09"
                        }else if datemonth == "Oct" {
                            self!.MonthStr = "10"
                        }else if datemonth == "Nov" {
                            self!.MonthStr = "11"
                        }else if datemonth == "Dec" {
                            self!.MonthStr = "12"
                        }
                       
                        self!.cardDTETF.text = dateString
                        self!.YearStr = dateyear
                        
                    }
                    
                    
                }
            }
        }
        calendar.preSelectedDate = selectedDate
        self.present(calendar, animated: false, completion: nil)
    }
    
    
    
    override var popupHeight: CGFloat { return height ?? CGFloat(300) }
    
    override var popupTopCornerRadius: CGFloat { return topCornerRadius ?? CGFloat(10) }
    
    override var popupPresentDuration: Double { return presentDuration ?? 1.0 }
    
    override var popupDismissDuration: Double { return dismissDuration ?? 1.0 }
    
    override var popupShouldDismissInteractivelty: Bool { return shouldDismissInteractivelty ?? true }
    
    override var popupDimmingViewAlpha: CGFloat { return BottomPopupConstants.kDimmingViewDefaultAlphaValue }

}

// MARK: - AlertController
extension PaymentPage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    
    
    
}
