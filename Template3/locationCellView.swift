//
//  locationCellView.swift
//  Template3
//
//  Created by TISSA Technology on 2/22/21.
//

import UIKit

class locationCellView: UITableViewCell {
    @IBOutlet weak var backgroundimage: UIImageView!
    @IBOutlet weak var locationname: UILabel!
    @IBOutlet weak var locationimage: UIImageView!
    @IBOutlet weak var locationdiscription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
