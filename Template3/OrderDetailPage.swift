//
//  OrderDetailPage.swift
//  Template3
//
//  Created by TISSA Technology on 2/24/21.
//

import UIKit
import Alamofire
import SDWebImage
import SideMenu

class OrderDetailPage: UIViewController {
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var producttable: UITableView!
    @IBOutlet weak var ordernoview: UIView!
    @IBOutlet weak var tableheight: NSLayoutConstraint!
    @IBOutlet weak var billingaddressBtn: UIButton!

    @IBOutlet weak var orderNoLbl: UILabel!
    @IBOutlet weak var orderDateLbl: UILabel!
    @IBOutlet weak var orderTotalLbl: UILabel!
    @IBOutlet weak var AddressShowLbl: UILabel!

    var fetchedItems = NSArray()
    var dateStr = String()
    var amtStr = String()
    var orderIDGet = Int()


    
    override func viewDidLoad() {
        super.viewDidLoad()

        view1.layer.cornerRadius = 5
        view1.layer.shadowColor = UIColor.lightGray.cgColor
        view1.layer.shadowOpacity = 1
        view1.layer.shadowOffset = .zero
        view1.layer.shadowRadius = 3
        
        view2.layer.cornerRadius = 5
        view2.layer.shadowColor = UIColor.lightGray.cgColor
        view2.layer.shadowOpacity = 1
        view2.layer.shadowOffset = .zero
        view2.layer.shadowRadius = 3
        
        ordernoview.layer.cornerRadius = 5
        billingaddressBtn.layer.cornerRadius = 10
        
        producttable.register(UINib(nibName: "detailproductCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        producttable.rowHeight = UITableView.automaticDimension
        producttable.estimatedRowHeight = 96

        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        orderitems()
        setdata()
        loadBillingAddress()
        
    }
    
    func setdata()  {
        
        orderDateLbl.text = dateStr
        orderTotalLbl.text = amtStr
        orderNoLbl.text = "Order No " + String(orderIDGet)
    }
    
    @IBAction func backClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
   
    }
  
}

// MARK: - uiTableViewDatasource
extension OrderDetailPage: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return fetchedItems.count
    
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! detailproductCell
        cell.selectionStyle = .none
     
        cell.outerview.layer.cornerRadius = 8
        cell.outerview.layer.shadowColor = UIColor.lightGray.cgColor
        cell.outerview.layer.shadowOpacity = 1
        cell.outerview.layer.shadowOffset = .zero
        cell.outerview.layer.shadowRadius = 3
        
        if fetchedItems.count == 0 {
            
        }else{
        
        let dictObj = self.fetchedItems[indexPath.row] as! NSDictionary

        cell.productname.text = dictObj["product_name"] as? String

            let rupee = "$"

            let pricedata = dictObj["line_total"]as! String

           cell.priceLblshow.text = rupee + pricedata
            
            var urlStr = String()

            if dictObj["product_url"] is NSNull {
                urlStr = ""
            }else{

                urlStr = dictObj["product_url"] as! String

            }

                let qtyLbl = "Qty : "
                
                let qnt = dictObj["quantity"] as! Int
                cell.quantityLbl.text = qtyLbl + String(qnt)

            let url = URL(string: urlStr )

           cell.productimage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.productimage.sd_setImage(with: url) { (image, error, cache, urls) in
                if (error != nil) {
                    // Failed to load image
                    cell.productimage.image = UIImage(named: "noimage.png")
                } else {
                    // Successful in loading image
                    cell.productimage.image = image
                }
            }

            
            let list:NSArray = dictObj.value(forKey: "ingredient") as! NSArray

            if list.count == 0 {

                cell.productdescription.isHidden = true
               
            }else{
                cell.productdescription.isHidden = false
               
                var indnamearr:[String] = []
                var indpricearr:[Double] = []
                var indpricearrforshow:[String] = []

                for (index, element1) in list.enumerated() {

                    let dictObj1 = list[index] as! NSDictionary
                    let ingredprice = dictObj1["line_total"]as! String
                    let ingredientname = dictObj1["ingredient_name"]as! String
                    let quantitystrrr = dictObj1["quantity"]as! Int
                    let convertqty = Int(quantitystrrr)

                    let nameshow = "\(index+1).\(ingredientname):\nPrice :$\(ingredprice)\nQty:\(convertqty)\n\n"

                    indpricearrforshow.append(nameshow)
                    indnamearr.append(ingredientname)
                    indpricearr.append(Double(ingredprice)!)
                }

                let total = indpricearr.reduce(0, +)

                print("ingredient total - \(total)")

                let rupee = "$"

                           let pricedata = dictObj["line_total"]as! String

                           let Pamt = Double(pricedata)

                           let Tamt =  Double(Pamt!) + total


                          cell.priceLblshow.text = rupee + String(format: "%.2f", Tamt)

        cell.productdescription.text = indpricearrforshow.map { String($0) }.joined(separator: "")

            }
            
            
        }
      //  cell.productdescription.text = descripArr[indexPath.row]
    
        
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
            return  UITableView.automaticDimension
        
    }
}

// MARK: - Api List
extension OrderDetailPage{
    
func orderitems()  {
    
    let defaults = UserDefaults.standard
    
  //  let admintoken = defaults.object(forKey: "adminToken")as? String
    
    let admintoken = defaults.object(forKey: "custToken")as? String
       
    
    let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
  //  let trimmedString = categoryStr.removingAllWhitespaces()
    
    let urlString = GlobalClass.DevlopmentApi+"order-item/?order_id=\(orderIDGet)"
    
   

        
    print("oderitems get url - \(urlString)")
    
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": autho
        ]

    AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 200{
                            
                                let dict1 :NSDictionary = response.value! as! NSDictionary
                                 
                               self.fetchedItems = (dict1.value(forKey:"results")as! NSArray)
                                
                                   print(self.fetchedItems)
                                
                                     if self.fetchedItems.count == 0 {
                                       
                                        self.producttable.reloadData()
                                        
                                        ERProgressHud.sharedInstance.hide()
                                     }else{
                                   
                                        tableheight.constant = producttable.estimatedRowHeight + 100 * CGFloat(fetchedItems.count)

                                        self.producttable.reloadData()
                                        
                                        ERProgressHud.sharedInstance.hide()

                                     }
                
                             
                            }else{
                                
                    if response.response?.statusCode == 401{
                                
                        ERProgressHud.sharedInstance.hide()
                    self.SessionAlert()
                      
                       }else if response.response?.statusCode == 500{
                                    
                        ERProgressHud.sharedInstance.hide()

                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                       }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                   }
                                
                                
                                
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()

                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow Internet Detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }
        
  
        
    }


func loadBillingAddress(){
    
    let defaults = UserDefaults.standard
    
 //   let admintoken = defaults.object(forKey: "adminToken")as? String
    let admintoken = defaults.object(forKey: "custToken")as? String
    let customerId = defaults.integer(forKey: "custId")
    
    let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
    
    let urlString = GlobalClass.DevlopmentApi + "billing/?customer_id=\(customerId)"
    
    print("category Url -\(urlString)")
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": autho
        ]

    AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
        response in
          switch response.result {
                        case .success:
                           // print(response)

                            if response.response?.statusCode == 200{
                             
                              
                                
                             let dict1 :NSDictionary = response.value! as! NSDictionary
                                
                                print("billing address - \(dict1)")
                              
                                let billingaddressData = (dict1.value(forKey:"results")as! NSArray)
                                
                                if billingaddressData.count == 0 {
                             
                           }else{

                 
                            let firstobj:NSDictionary  = billingaddressData.object(at: 0) as! NSDictionary
                           
                            let addline1Lbl = firstobj["name"]as! String
                            
                            let one = firstobj["address"]as! String
                            let two = firstobj["house_number"]as! String
                            let addline2Lbl = one + " " + two
                            let addline3Lbl = firstobj["city"]as! String
                            let addline4Lbl = firstobj["state"]as! String
                            let addline5Lbl = firstobj["country"]as! String
                            let addline6Lbl = firstobj["zip"] as! String
                             
                            AddressShowLbl.text = addline1Lbl + ", " + addline2Lbl + ", " + addline3Lbl + ", " + addline4Lbl + ", " + addline5Lbl + ", " + addline6Lbl
                           }
                                
                              
                             
                            }else{
                                
                                if response.response?.statusCode == 401{
                                
                                    ERProgressHud.sharedInstance.hide()

                                    self.SessionAlert()
                                    
                                }else if response.response?.statusCode == 404{
                                    
                                   
                                    
                                    
                                }else if response.response?.statusCode == 500{
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                    
                                }
                                
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()
                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow Internet Detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }
        
  
        
    }
}

// MARK: - AlertController
extension OrderDetailPage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}
