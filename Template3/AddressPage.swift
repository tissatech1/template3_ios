//
//  AddressPage.swift
//  Template3
//
//  Created by TISSA Technology on 2/22/21.
//

import UIKit
import Alamofire
import SDWebImage
import SideMenu

class AddressPage: UIViewController {

    
    @IBOutlet weak var exixtingaddBtn: UIButton!
    @IBOutlet weak var exixtingAddLbl: UILabel!
    @IBOutlet weak var fullnameTf: UITextField!
    @IBOutlet weak var addresslineTf: UITextField!
    @IBOutlet weak var housenumberTf: UITextField!
    @IBOutlet weak var cityTf: UITextField!
    @IBOutlet weak var zipTf: UITextField!
    @IBOutlet weak var countryTf: UITextField!
    @IBOutlet weak var stateTf: UITextField!
    @IBOutlet weak var notefullView: UIView!
    @IBOutlet weak var notehalfView: UIView!
    @IBOutlet weak var fullnameTopCont: NSLayoutConstraint!
    @IBOutlet weak var blurview: UIView!
    @IBOutlet weak var stateTable: UITableView!
    @IBOutlet weak var feesection: UIView!

    @IBOutlet weak var subtotalLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    
    var discoutStr = String()
    var servicefeeStr = String()
    var shippoingfeeStr = String()
    var subtotalStr = String()
    var taxStr = String()
    var totalStr = String()
    var tipStr = String()
    
    
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    
    var StateDict = NSArray()
    var nameadd = NSString()
    var addressadd = NSString()
    var hounseadd = NSString()
    var cityadd = NSString()
    var stateadd = NSString()
    var countryadd = NSString()
    var sipadd = NSString()
    var companynameadd = NSString()
    var billId = NSString()
    var passbillingdata = NSDictionary()
    var billingaddressData = NSArray()
    var billing = NSString()
    var useexistadd = NSString()
    var shippingMethod = NSArray()
    
    var orderresultCurrency = String()
    var orderresultamount = String()
    var orderresultorderid = Int()
    var orderresultcustomerid = Int()
    var customerPhoneNumber = String()
    var customerEmailId = String()
    var stringoOfCArdNO = String()
    var stringOfACardCVC = String()
    var stingOFCardMonth = String()
    var stringOfCardYear = String()
    var YearArr = NSArray()
    var whichbtn = String()
    var dates =  [Date]()
    var shippingmethodStr = String()
    var paymentmethodStr = String()
    var customerfullname = String()
    var orderplaced = String()
    
    let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        blurview.isHidden = true
        feesection.isHidden = true
        loadState()
        getcustomer()
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        loadshippingMethod()
        loadBillingAddress()
        
        stateTable.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        
        GlobalClass.PayClicked = "no"

        
    }
    
    @IBAction func placeorderClicked(_ sender: Any) {
        
        guard let popupVC = storyboard?.instantiateViewController(withIdentifier: "PaymentPage") as? PaymentPage else { return }
        popupVC.height = self.view.frame.height / 1.3
        popupVC.topCornerRadius = 20
        popupVC.presentDuration = 0.5
        popupVC.dismissDuration = 0.5
        popupVC.shouldDismissInteractivelty = true
        popupVC.popupDelegate = self
        
        
        
        present(popupVC, animated: true, completion: nil)
        
   
    }
    
    @IBAction func backClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
   
    }
    
    
    @IBAction func statebutnClicked(_ sender: Any) {
        blurview.isHidden = false
        
    }
    
    @IBAction func blurcancelClicked(_ sender: Any) {
        
        blurview.isHidden = true
        
    }
    
    
    @IBAction func existingaddBtnClicked(_ sender: Any) {
        
        if exixtingaddBtn.tag == 0 {
           
            feesection.isHidden = false
            
            let image = UIImage(named: "right.png") as UIImage?
            self.exixtingaddBtn.setBackgroundImage(image, for: .normal)
            
            self.fullnameTf.isUserInteractionEnabled = false
            self.addresslineTf.isUserInteractionEnabled = false
            self.housenumberTf.isUserInteractionEnabled = false
            self.cityTf.isUserInteractionEnabled = false
            self.zipTf.isUserInteractionEnabled = false
            self.countryTf.isUserInteractionEnabled = false
            self.stateTf.isUserInteractionEnabled = false
            
            exixtingaddBtn.tag = 1
            self.useexistadd = "yes"
            
        }else{
          
            feesection.isHidden = true
            
            let image = UIImage(named: "blank-square.png") as UIImage?
            self.exixtingaddBtn.setBackgroundImage(image, for: .normal)
            
            self.fullnameTf.isUserInteractionEnabled = true
            self.addresslineTf.isUserInteractionEnabled = true
            self.housenumberTf.isUserInteractionEnabled = true
            self.cityTf.isUserInteractionEnabled = true
            self.zipTf.isUserInteractionEnabled = true
            self.countryTf.isUserInteractionEnabled = true
            self.stateTf.isUserInteractionEnabled = true
            
            exixtingaddBtn.tag = 0
            self.useexistadd = "no"
            
        }
        
    }
    
    
    
    @IBAction func billingnextBtnClicked(_ sender: Any) {
        
        if billing == "no" {
        

            print("fulladd=\(fullnameTf.text ?? "nodata")")
            
                if fullnameTf.text == "" || fullnameTf.text == nil {
                    self.showSimpleAlert(messagess: "Enter full name")
                }else if addresslineTf.text == nil || addresslineTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter address line")
                }else if cityTf.text == nil || cityTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter city")
                }else if zipTf.text == nil || zipTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter zip code")
                    
                    
                }else if countryTf.text == nil || countryTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter country")
                    
                    
                }else if stateTf.text == nil || stateTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter state")
                    
                    
                }else{
                    
                
                self.dataset()
                
            }
            
            
        }else{
            
            if self.useexistadd == "no" {
                
                print("fulladd=\(fullnameTf.text ?? "nodata")")
                

                if fullnameTf.text == "" || fullnameTf.text == nil {
                    self.showSimpleAlert(messagess: "Enter full name")
                }else if addresslineTf.text == nil || addresslineTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter address line")
                }else if cityTf.text == nil || cityTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter city")
                }else if zipTf.text == nil || zipTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter zip code")
                }else if countryTf.text == nil || countryTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter country")
                    
                    
                }else if stateTf.text == nil || stateTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter state")
                    
                    
                }else{
                    
                    self.dataset()
                    
                }
                
                
            }else{
               
//                let home = self.storyboard?.instantiateViewController(withIdentifier: "PaymentPage") as! PaymentPage
//                self.navigationController?.pushViewController(home, animated: true)
//
                    
                }

                
            }
            

        let defaults = UserDefaults.standard
        defaults.set(passbillingdata, forKey: "billingaddressDICT")
           
        }
        
        
    func dataset()  {
        
        if self.billing == "no" {
            
            self.nameadd = fullnameTf.text! as NSString
            self.addressadd = addresslineTf.text! as NSString
            self.hounseadd = housenumberTf.text! as NSString
            self.cityadd = cityTf.text! as NSString
            self.stateadd = stateTf.text! as NSString
            if countryTf.text == "India" {
                self.countryadd = "IN"
            }else{
                
                self.countryadd = "US"
            }
            
            self.sipadd = zipTf.text! as NSString
            ERProgressHud.sharedInstance.show(withTitle: "Loading...")
            self.addBillingAddress()
            
        }else{
            
            if self.useexistadd == "no" {
                
                self.nameadd = fullnameTf.text! as NSString
                self.addressadd = addresslineTf.text! as NSString
                self.hounseadd = housenumberTf.text! as NSString
                self.cityadd = cityTf.text! as NSString
                self.stateadd = stateTf.text! as NSString
                if countryTf.text == "India" {
                    self.countryadd = "IN"
                }else{
                    
                    self.countryadd = "US"
                }
                
                self.sipadd = zipTf.text! as NSString
                ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                self.updateBillingAddress()
                
            }else{
                
                
            }
            
            
        }
        
        
    }
    

}

extension AddressPage: BottomPopupDelegate {
    
    func bottomPopupViewLoaded() {
        print("bottomPopupViewLoaded")
    }
    
    func bottomPopupWillAppear() {
        print("bottomPopupWillAppear")
    }
    
    func bottomPopupDidAppear() {
        print("bottomPopupDidAppear")
    }
    
    func bottomPopupWillDismiss() {
        print("bottomPopupWillDismiss")
//        let summary = self.storyboard?.instantiateViewController(withIdentifier: "summaryPage") as! summaryPage
//        self.navigationController?.pushViewController(summary, animated: true)
        
        if GlobalClass.PayClicked == "yes" {
           
         print("payment")
            ERProgressHud.sharedInstance.show(withTitle: "Loading...")

            orderDetailApi()
            
        }else{
            
            
        }
        
        
    }
    
    func bottomPopupDidDismiss() {
        print("bottomPopupDidDismiss")
    }
    
    func bottomPopupDismissInteractionPercentChanged(from oldValue: CGFloat, to newValue: CGFloat) {
        print("bottomPopupDismissInteractionPercentChanged fromValue: \(oldValue) to: \(newValue)")
    }
}


// MARK: - AlertController
extension AddressPage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}

// MARK: - Api
extension AddressPage {
    
    func loadBillingAddress(){
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
        let urlString = GlobalClass.DevlopmentApi + "billing/?customer_id=\(customerId)"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "billing"
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                 
                                  
                                    
                                 let dict1 :NSDictionary = response.value! as! NSDictionary
                                    
                                    print("billing address - \(dict1)")
                                  
                                self.billingaddressData = (dict1.value(forKey:"results")as! NSArray)
                                    
                                    
                                    
                                    if self.billingaddressData.count == 0 {
                                     
                                      self.billing = "no"
                                        self.useexistadd = "no"
                                      
                                        if billing == "no" {
                                            
                                            self.notefullView.isHidden = true
                                            self.notehalfView.isHidden = true
                                            self.exixtingaddBtn.isHidden = true
                                            self.exixtingAddLbl.isHidden = true
                                            self.fullnameTopCont.constant = 12
                                            
                                        }else{
                                            
                                            self.notefullView.isHidden = false
                                            self.notehalfView.isHidden = false
                                            self.exixtingaddBtn.isHidden = false
                                            self.exixtingAddLbl.isHidden = false
                                            self.fullnameTopCont.constant = 169
                                            
                                        }
                                        
                                           ERProgressHud.sharedInstance.hide()

                                        
                                   
                               }else{
                                  
                                fisrttimefeeapi()
                                     
                                  self.billing = "yes"
                                self.useexistadd = "yes"
                                  
                                  let firstobj:NSDictionary  = self.billingaddressData.object(at: 0) as! NSDictionary
                                  
                                let defaults = UserDefaults.standard
                                defaults.set(firstobj, forKey: "billingaddressDICT")
                                
                                self.passbillingdata = firstobj
                                
                                self.nameadd = firstobj["name"]as! NSString
                                self.addressadd = firstobj["address"]as! NSString
                                self.hounseadd = firstobj["house_number"]as! NSString
                                self.cityadd = firstobj["city"]as! NSString
                                self.stateadd = firstobj["state"]as! NSString
                                self.countryadd = firstobj["country"]as! NSString
                                self.sipadd = firstobj["zip"]as! NSString
                                  
                                let shipId = firstobj["id"] as! Int

                                 self.billId = String(shipId) as NSString
                                
                                  
                                self.exixtingAddLbl.text = "\(self.nameadd)\n"+"\(self.addressadd), "+"\(self.hounseadd)\n"+"\(self.cityadd), "+"\(self.stateadd), "+"\(self.countryadd),\n"+"\(self.sipadd)."
                                  
                                  
                                  let image = UIImage(named:"right.png") as UIImage?
                                  self.exixtingaddBtn.setBackgroundImage(image, for: .normal)
                                  self.exixtingaddBtn.tag = 1
                                  
                                  self.fullnameTf.isUserInteractionEnabled = false
                                  self.addresslineTf.isUserInteractionEnabled = false
                                  self.housenumberTf.isUserInteractionEnabled = false
                                  self.cityTf.isUserInteractionEnabled = false
                                  self.zipTf.isUserInteractionEnabled = false
                                  self.countryTf.isUserInteractionEnabled = false
                                  self.stateTf.isUserInteractionEnabled = false
                      
                              //  ERProgressHud.sharedInstance.hide()

                               }
                                     
                                 
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 404{
                                        
                                       
                                        
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                        
                                    }
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                                    self.showSimpleAlert(messagess:"Slow Internet Detected")
                                            
                                        }else{
                                        
                                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                        }

                                           print(error)
                                    }
            }
            
      
            
        }
    
    
    func loadState(){
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
      //  let country = "United States of America"
       // let trimmed = country.trimmingCharacters(in: .whitespacesAndNewlines)
     //   let trimmed = country.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let urlString = GlobalClass.DevlopmentApi + "tax/?country_code=US"
        
      //  let trimmed = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "billing"
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                    
                                    let dict1 :NSDictionary = response.value! as! NSDictionary
                                       
                                       print("billing address - \(dict1)")
                                     
                                    
                                    
                                    let arraysh: NSArray  = (dict1.value(forKey:"results")as! NSArray)
                                    
                                    if arraysh.count == 0 {
                                       
                                        self.StateDict = []
                                        
                                        stateTable.reloadData()
                                        
                                    }else{
                                        
                                        self.StateDict = arraysh
                                        
                                       stateTable.reloadData()

                                    }
                                    
                                 //   ERProgressHud.sharedInstance.hide()

                                    
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 404{
                                        
                                       
                                        
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                        
                                    }
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                                    self.showSimpleAlert(messagess:"Slow Internet Detected")
                                            
                                        }else{
                                        
                                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                        }

                                           print(error)
                                    }
            }
            
      
            
        }
    
   
    func loadshippingMethod(){
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let storeId = defaults.object(forKey: "clickedStoreId")as? String
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
        let urlString = GlobalClass.DevlopmentApi + "shipping-method/?status=ACTIVE&restaurant=\(storeId ?? "101")"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

             AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                 
                                  
                                    
                                 let dict1 :NSDictionary = response.value! as! NSDictionary
                                    
                                    print("shipping Method - \(dict1)")
                                  
                                self.shippingMethod = (dict1.value(forKey:"results")as! NSArray)
                                    
                                    print("shipping Method result - \(self.shippingMethod)")
                                    
                                      if self.shippingMethod.count == 0 {
                                       
                                        ERProgressHud.sharedInstance.hide()

//                                        self.viewhide()
//                                        self.shippingmethodfullview.isHidden = true
//                                        self.storeselectLbl.isHidden = true
//                                        self.homeselctLbl.isHidden = true
                                        
                                        self.showSimpleAlert(messagess: "No shipping method available")
                                        
                                     
                                 }else{
                                       
                                    let dictObj = self.shippingMethod[0] as! NSDictionary
                                       
                                    let status = dictObj["name"]
                                    let statusid = dictObj["id"]
                                    
                                    defaults.set(status, forKey: "clickedShippingMethod")
                                    defaults.set(statusid, forKey: "clickedShippingMethodId")
                        
                                    ERProgressHud.sharedInstance.hide()

                                 }
                                     
                                 
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func fisrttimefeeapi() {
        
        
         let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)

      //  let getshippingId = defaults.object(forKey: "clickedShippingMethodId")as! Int
        
        var subtotalStrfee = String()
        var notaxtotalstr = String()
     //  let carttotal = defaults.object(forKey: "totalcartPrice")as! String
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

        let cartarray:NSArray = defaults.object(forKey: "cartarray")as! NSArray

        print("cart array - \(cartarray)")
        
       // var sumnotax = 0.00
        var sumsub = 0.00
           
        notaxtotalstr = "0.00"
        subtotalStrfee = "0.00"
        
        for i in 0 ..< cartarray.count  {

         //   let datagetobject:NSDictionary  = cartarray.object(at: i) as! NSDictionary
        
           
//            if (datagetobject["tax_exempt"] as! Bool == true) {
//
//                let countobj:NSDictionary  = cartarray.object(at: i) as! NSDictionary
//
//                let linetotal = countobj["line_total"] as! String
//
//
//                let qnt = Double(linetotal)
//
//
//                sumnotax = sumnotax + qnt!
//
//                notaxtotalstr = String(format: "%.2f", sumnotax)
//
//
//
//            }else{
//
               
                
//                let countobj:NSDictionary  = cartarray.object(at: i) as! NSDictionary
//
//                let linetotal = countobj["line_total"] as! String
//
//
//                let qnt = Double(linetotal)
//
//
//                sumsub = sumsub + qnt!
//
//
//                subtotalStrfee = String(format: "%.2f", sumsub)

          //  }
        
        
        }
        
        let passedcartprice = defaults.object(forKey: "totalcartPrice")as? String
        
        subtotalStrfee = passedcartprice!
    
        print("customerId - \(customerId)")
    //    print("getshippingId - \(getshippingId)")
        print("carttotal - \(subtotalStrfee)")
        
        print("no_tax_total - \(notaxtotalstr)")
        print("sub_total - \(subtotalStrfee)")
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "fee"
            ]

               let urlString = GlobalClass.DevlopmentApi+"fee/"

        AF.request(urlString, method: .post, parameters: ["sub_total": subtotalStrfee, "no_tax_total": notaxtotalstr,"customer_id": customerId,"tip": "0","restaurant":GlobalClass.restaurantGlobalid],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
               response in
                 switch response.result {
                               case .success:
                                print(response)

                                   if response.response?.statusCode == 200{
                                    
                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    feesection.isHidden = false

                                    
                                     print("firstTimefee Result - \(dict)")
                                       
   
                                        self.discountLbl.text = "$\((dict["discount"] as! String))"
                                        discoutStr = (dict["discount"] as! String)
                                        
                              //  self.servicefeelbl.text = "$\((dict["service_fee"] as! String))"
                                        servicefeeStr = (dict["service_fee"] as! String)

                                        
                             //   self.shippingfeeLbl.text = "$\((dict["shipping_fee"] as! String))"
                                        shippoingfeeStr = (dict["shipping_fee"] as! String)


                            self.subtotalLbl.text = "$\((dict["sub_total"] as! String))"
                                        subtotalStr = (dict["sub_total"] as! String)

                                        
                            self.taxLbl.text = "$\((dict["tax"] as! String))"
                                        taxStr = (dict["tax"] as! String)

                                        
                            self.totalLbl.text = "$\((dict["total"] as! String))"
                                        totalStr = (dict["total"] as! String)

                                       
                                    ERProgressHud.sharedInstance.hide()

                                    
                                   }else{
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    if response.response?.statusCode == 401{
                                        
                                        ERProgressHud.sharedInstance.hide()
                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                
                                    
                                   }
                                   
                                   break
                               case .failure(let error):
                                
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                               }
               }


            }
    
    
    func addBillingAddress()  {
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let urlString = GlobalClass.DevlopmentApi+"billing/"
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "billing"
            ]
        

        AF.request(urlString, method: .post, parameters: ["name":self.nameadd,"company_name":self.companynameadd,"address":self.addressadd,"house_number":self.hounseadd,"zip":self.sipadd,"city":self.cityadd,"country":self.countryadd,"state":self.stateadd,"customer_id":"\(customerId)","priority":1],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 201{
                             
                             let dict :NSDictionary = response.value! as! NSDictionary
                              print("add billing responce - \(dict)")
                                
                                
                                self.passbillingdata = dict
                                
                                let defaults = UserDefaults.standard
                                defaults.set(self.passbillingdata, forKey: "billingaddressDICT")

//                                ERProgressHud.sharedInstance.hide()
//
//                                let csrt = self.storyboard?.instantiateViewController(withIdentifier: "PaymentPage") as! PaymentPage
//                                self.navigationController?.pushViewController(csrt, animated: true)

                                self.fisrttimefeeapi()
                                
                               
                            }else{
                                
                                if response.response?.statusCode == 401{
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                    
                                }else if response.response?.statusCode == 500{
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                    
                                }
                                
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()
                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            
                            if error.localizedDescription == msg {
                                
                                self.showSimpleAlert(messagess:"No internet connection")
                                
                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                                self.showSimpleAlert(messagess:"Slow Internet Detected")
                                        
                                    }else{
                                    
                                        self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                }
        }

        
        
    }
    
    func updateBillingAddress()  {
        
        let  idd = Int(billId as String)
        
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let urlString = GlobalClass.DevlopmentApi+"billing/\(idd ?? 0)/"
        
        print("update query - \(urlString)")
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "billing"
            ]
        

        AF.request(urlString, method: .put, parameters: ["name":self.nameadd,"company_name":self.companynameadd,"address":self.addressadd,"house_number":self.hounseadd,"zip":self.sipadd,"city":self.cityadd,"country":self.countryadd,"state":self.stateadd,"customer_id":"\(customerId)","priority":1],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 200{
                             
                             let dict :NSDictionary = response.value! as! NSDictionary
                              print("update billing responce - \(dict)")
                                
                                ERProgressHud.sharedInstance.hide()

                                self.passbillingdata = dict
                                let defaults = UserDefaults.standard
                                defaults.set(self.passbillingdata, forKey: "billingaddressDICT")
                               
//                                ERProgressHud.sharedInstance.hide()
//
//                                let csrt = self.storyboard?.instantiateViewController(withIdentifier: "PaymentPage") as! PaymentPage
//                                self.navigationController?.pushViewController(csrt, animated: true)

                                
                                self.fisrttimefeeapi()
                                
                               
                            }else{
                                
                                if response.response?.statusCode == 401{
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                    
                                }else  if response.response?.statusCode == 500{
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                    
                                }
                                
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()
                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            
                            if error.localizedDescription == msg {
                                
                                self.showSimpleAlert(messagess:"No internet connection")
                                
                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                                self.showSimpleAlert(messagess:"Slow Internet Detected")
                                        
                                    }else{
                                    
                                        self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                }
        }

        
        
    }
    
    func getcustomer()  {
        
        let defaults = UserDefaults.standard
        
        let savedUserData = defaults.object(forKey: "custToken")as? String
        
        let customerid = defaults.integer(forKey: "custId")
        let custidStr = String(customerid)
        
        let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
            
        let urlString = GlobalClass.DevlopmentApi+"customer/?customer_id="+custidStr+""
        print("Url cust avl - \(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": token
            ]

             AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                
                                print(response)

                                if response.response?.statusCode == 200{
                                    ERProgressHud.sharedInstance.hide()

                                    let dict :NSDictionary = response.value! as! NSDictionary
                                                //   print(dict)
                                    
                                    let status = dict.value(forKey: "results")as! NSArray
                                                 //  print(status)

                                        print("customer detail - \(status)")

                                    let firstobj:NSDictionary  = status.object(at: 0) as! NSDictionary
                                    
                                    self.customerPhoneNumber = firstobj["phone_number"] as! String
                                    
                                    print(self.customerPhoneNumber)
                                    
                                    let customerinfo:NSDictionary = firstobj.value(forKey: "customer")as! NSDictionary
                                    
                                    
                                    self.customerEmailId = customerinfo["email"] as! String
                                    
                                //    self.customerEmailId = ""
                                    
                                    print(self.customerEmailId)
                                    
                               
                                    let firstnamesh = customerinfo["first_name"] as! String
                                    let lastnamesh = customerinfo["last_name"] as! String

                                    
                                    self.customerfullname = firstnamesh + " " + lastnamesh
                                    
                               
                                    
                                    
                      }else{
                                    
                                //    self.dissmiss()
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        
                                    }else if response.response?.statusCode == 401{
                                        ERProgressHud.sharedInstance.hide()

                                    self.SessionAlert()
                                        
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                            
                                print(response)
                                }
                                
                                break
                            case .failure(let error):
                                
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func orderDetailApi() {


       let discountdata = discoutStr

        let servicefeedata = servicefeeStr

      //  let servicefeedata = "0.00"

        let shippingfeedata = shippoingfeeStr

        let subtotaldata = subtotalStr

        let taxdata = taxStr

        let totaldata =  totalStr

        let tipdata = "0"
        
        var commentStr = ""
        
      
          let  cuurency = "USD"
        

       // let cuurency = globelObjectVC.countryfixglob
        print("get currency - \(cuurency)")
        var customrtid = String()

         let defaults = UserDefaults.standard

     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)

        customrtid = String(customerId)

        let getshippingId = defaults.object(forKey: "clickedShippingMethodId")as! Int
       let carttotal = defaults.object(forKey: "totalcartPrice")as! String

        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"


        print("customerId - \(customerId)")
        print("getshippingId - \(getshippingId)")
        print("carttotal - \(carttotal)")

        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)

            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "order-detail"
            ]

               let urlString = GlobalClass.DevlopmentApi+"order-detail/"

        AF.request(urlString, method: .post, parameters: ["status": "active", "currency": cuurency,"subtotal": subtotaldata,"total": totaldata,"extra": commentStr, "customer": customrtid,"tip": tipdata,"service_fee": servicefeedata,"tax": taxdata,"discount": discountdata,"shipping_fee": shippingfeedata,"cart_id": cartidStr],encoding: JSONEncoding.default, headers: headers).responseJSON {
               response in
                 switch response.result {
                               case .success:
                                 //  print(response)

                                   if response.response?.statusCode == 200{

                                    self.orderplaced = "yes"
                                    
                                    let dict :NSDictionary = response.value! as! NSDictionary

                                     print("order detail Result - \(dict)")

                                    let defaults = UserDefaults.standard
                                    defaults.set(dict, forKey: "passOrderResult")

                                    self.orderresultCurrency = (dict["currency"] as! String)
                                    self.orderresultamount = (dict["total"] as! String)
                                    self.orderresultorderid = (dict["order_id"] as! Int)
                                    self.orderresultcustomerid = (dict["customer"] as! Int)


                                   
                                   // self.show()
                                    self.paymentApi()
                                    
                                   
                                    

                                   }else{

                                    ERProgressHud.sharedInstance.hide()

                                    if response.response?.statusCode == 401{

                                        ERProgressHud.sharedInstance.hide()
                                        self.SessionAlert()

                                    }else if response.response?.statusCode == 500{

                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary

                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }



                                   }

                                   break
                               case .failure(let error):
                                
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                               }
               }


            }

    func paymentApi()  {
        
        
        let defaults = UserDefaults.standard
       
    //   let admintoken = defaults.object(forKey: "adminToken")as? String
       let admintoken = defaults.object(forKey: "custToken")as? String
       let customerId = defaults.integer(forKey: "custId")
        let  customeridStr = String(customerId)

        
       let getshippingId = defaults.object(forKey: "clickedShippingMethodId")as! Int
        let shippingmethodName = defaults.object(forKey: "clickedShippingMethod")as! String
        
        print(shippingmethodName)
        let shippingmethodID = defaults.integer(forKey: "clickedShippingMethodId")
        
       let storepassid = (defaults.object(forKey: "clickedStoreId")as? String)!
        
        print(storepassid)
       
        var commentStr = ""
        
       print("commentStr - \(commentStr)")
      
       let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

      
        
        var metadataDict = [String : Any]()
        
        metadataDict = ["order_id":orderresultorderid, "restaurant_id":storepassid, "customer_id":orderresultcustomerid, "shippingmethod_id":shippingmethodID,"phone":customerPhoneNumber,"name":customerfullname,"special_instruction":commentStr]
        
        print(metadataDict)
        
        
        
        var cardDataDict = [String : Any]()
        
        cardDataDict = ["number":GlobalClass.cardNumber, "exp_month":GlobalClass.monthNumber,"exp_year":GlobalClass.yearNumber, "cvc":GlobalClass.CvvNumber]
        
        print(cardDataDict)
        
        
        let billinfo:NSDictionary = defaults.object(forKey: "billingaddressDICT")as! NSDictionary

        print("billinfo dict - \(billinfo)")
        
        let cityStr = billinfo["city"] as! String
        
        let address2 = "\(billinfo["house_number"] as! String)" + "\(billinfo["address"] as! String)"
        
        let address1 = billinfo["company_name"] as! String
        
        let postalStr = billinfo["zip"] as! String
        
        let stateStr = billinfo["state"] as! String
        
        let addressDict = ["city":cityStr, "line1":address1,"line2":address2, "postal_code":postalStr,"state":stateStr]
        
        
        var addressDataDict = [String : Any]()
        
        addressDataDict = ["address":addressDict]
        print(addressDataDict)
   
       print("customerId - \(customerId)")
       print("getshippingId - \(getshippingId)")
        print("addresspassed - \(addressDataDict)")
       
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let orderidStrheader = String(orderresultorderid)
           
           let headers: HTTPHeaders = [
               "Content-Type": "application/json",
               "Authorization": autho,
               "order_id": orderidStrheader,
               "user_id": customeridStr,
               "cart_id": cartidStr,
               "action": "payment"
           ]

              let urlString = GlobalClass.DevlopmentApi+"payment/"

       AF.request(urlString, method: .post, parameters: ["currency": "USD", "amount": orderresultamount,"receipt_email": customerEmailId,"type": "card","card": cardDataDict,"billing_details": addressDataDict,"metadata": metadataDict],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
              response in
                switch response.result {
                              case .success:
                                 // print(response)

                                  if response.response?.statusCode == 200{
                                   
                                   let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    print(dict)
                                    
//                                    let shippingMDict:NSDictionary = dict["shippingmethod"]as! NSDictionary
//
//                                    shippingmethodStr = shippingMDict["name"]as! String
                           //         paymentmethodStr = dict["payment_method"]as! String
                                  
                                    print("payment Resultshow - \(dict)")
                                    ERProgressHud.sharedInstance.hide()

                                    let alert = UIAlertController(title:nil , message: "Order placed successfully",         preferredStyle: UIAlertController.Style.alert)

                                  
                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in
                                                                    
                                                                    let home = self.storyboard?.instantiateViewController(withIdentifier: "summaryPage") as! summaryPage
                                                                    
                                                home.orderIDGet = self.orderresultorderid
                                                                    
                                                                    self.navigationController?.pushViewController(home, animated: true)
                                        
                                                                    
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor.black
                                   
                                    
                                   
                                  }else{
                                   
                                    ERProgressHud.sharedInstance.hide()

                                    if response.response?.statusCode == 400{
                                        
                                        ERProgressHud.sharedInstance.hide()
                                        self.showSimpleAlert(messagess: "Incorrect card details. Please check")
                                        
                                    }else if response.response?.statusCode == 401{
                                       
                                        ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                       
                                   }else if response.response?.statusCode == 500{
                                       
                                    ERProgressHud.sharedInstance.hide()

                                       let dict :NSDictionary = response.value! as! NSDictionary
                                       
                                       self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                   }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                   }
                                   
                               
                                   
                                  }
                                  
                                  break
                              case .failure(let error):
                                
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                              }
              }


           }
    
}


// MARK: - tableview dtasource and delegates
extension AddressPage : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return StateDict.count
      
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

      //  let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)

        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
         let dictObj = self.StateDict[indexPath.row] as! NSDictionary

        cell.textLabel!.text = dictObj["state"] as? String

        cell.textLabel?.textAlignment = .center

        return cell
    }
    

    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
          
            return 45
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        let dictObj = self.StateDict[indexPath.row] as! NSDictionary

        blurview.isHidden = true
        stateTf.text = dictObj["state"] as? String
    }
    
    
    
}
