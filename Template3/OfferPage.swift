//
//  OfferPage.swift
//  Template3
//
//  Created by TISSA Technology on 2/25/21.
//

import UIKit
import SideMenu
import Alamofire
import SDWebImage

class OfferPage: UIViewController {
    
    @IBOutlet weak var offertable: UITableView!
    
    var OffersList = NSArray()
    var passproductid = Int()
    var quantityNumber = Int()
    var restStatusStr = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        offertable.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        gettiming()
       
        offertable.rowHeight = UITableView.automaticDimension
        offertable.estimatedRowHeight = 197

    }
    

    @IBAction func backClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
   
    }
}

// MARK: - uiTableViewDatasource
extension OfferPage: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return OffersList.count
         

    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuCell
        cell.selectionStyle = .none
        
        cell.outerview.layer.cornerRadius = 8
        cell.outerview.layer.shadowColor = UIColor.lightGray.cgColor
        cell.outerview.layer.shadowOpacity = 1
        cell.outerview.layer.shadowOffset = .zero
        cell.outerview.layer.shadowRadius = 3
        cell.addtocartBtn.layer.cornerRadius = 10
        cell.dishimage.layer.masksToBounds = false
        cell.dishimage.layer.cornerRadius = 8
        cell.dishimage.clipsToBounds = true
        cell.blurview.layer.cornerRadius = 8

       cell.addtocartBtn.tag = indexPath.row
       cell.addtocartBtn.addTarget(self, action: #selector(addBtnCLicked(_:)), for: .touchUpInside)
        
        let dictObj = self.OffersList[indexPath.row] as! NSDictionary
       
        var urlStr = String()
        if dictObj["product_url"] is NSNull || dictObj["product_url"] == nil{

                    urlStr = ""

                }else{
                    urlStr = dictObj["product_url"] as! String
                }
              
                let url = URL(string: urlStr )


        cell.dishimage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.dishimage.sd_setImage(with: url) { (image, error, cache, urls) in
                    if (error != nil) {
                        // Failed to load image
                        cell.dishimage.image = UIImage(named: "noimage.png")
                    } else {
                        // Successful in loading image
                        cell.dishimage.image = image
                    }
                }
        
        cell.descriptionLbl.text = dictObj["extra"] as? String
        cell.dishnameLbl.text = dictObj["product_name"] as? String
        var conprice = String()
        let rupee = "$"
        if let pricedata = dictObj["price"] as? String {
             conprice = pricedata
        }else if let pricedata = dictObj["price"] as? NSNumber {
            
             conprice = pricedata.stringValue
        }
    
        cell.priceshowLbl.text = rupee + conprice
        
        return cell
    }
    
        @IBAction func addBtnCLicked(_ sender: UIButton) {
    
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
               
                let alert = UIAlertController(title:nil, message: "Please login to continue. We can't let just anyone have this access to ordering!",         preferredStyle: UIAlertController.Style.alert)

                alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { _ in
                   
                }))
                alert.addAction(UIAlertAction(title: "SIGN IN",
                                              style: UIAlertAction.Style.default,
                                              handler: {(_: UIAlertAction!) in
                                                let home = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                                self.navigationController?.pushViewController(home, animated: true)
                                                
                }))
                
                self.present(alert, animated: true, completion: nil)
                alert.view.tintColor = UIColor.black
                
            }else{
                if restStatusStr == "open" {
            
            let dictObj = self.OffersList[sender.tag] as! NSDictionary
            
            passproductid = dictObj["product_id"] as! Int
            quantityNumber = 1
            
            ERProgressHud.sharedInstance.show(withTitle: "Loading...")

            addToCart()
                }else{
                    
                }
                
            }
    
        }
    
   
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
            return  UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
}

// MARK: - Api LIst
extension OfferPage {
    
    //MARK: Webservice Call timing
        
        
        func gettiming(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
            }else{
                admintoken = (defaults.object(forKey: "custToken")as? String)!
            }
            
            let autho = "token \(admintoken)"
            
            let urlString = GlobalClass.DevlopmentApi+"hour/?restaurant_id=\(GlobalClass.restaurantGlobalid)"
            
               
            print(" categoryurl - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                    print(response)

                                    if response.response?.statusCode == 200{
                                        
                                        let dict :NSDictionary = response.value! as! NSDictionary
                                       
                                        let data  = dict["status"]as! String
                                        
                                        self.restStatusStr = data
                                        
                                      //  self.restStatusStr = "open"
                                        
                                       // data = "closed"
                                        
                                        if data == "closed" {
                                         
                                            print("restaurant is - \(data)")
                                            
//                                            self.categoryshNameLbl.text = "Closed"
//
//                                            self.menulistView.alpha = 0.5
//
//                                            self.categoryview.alpha = 0.5
                                            
                                        }else{
                                        
                                            
                                            print("restaurant is - open")
                                        }
                                        
                                        self.GetOfferList()
                                        
                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {

                                self.showSimpleAlert(messagess:"No internet connection")

                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                                self.showSimpleAlert(messagess:"Slow Internet Detected")

                                    }else{
                                    
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                    
                                    
                                    
                                }
                }
                
          
                
            }
    
   
  
    //MARK: Webservice Call for Offers
        
        func GetOfferList(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
            }else{
                admintoken = (defaults.object(forKey: "custToken")as? String)!
            }
            let autho = "token \(admintoken)"
            var urlString = String()

            urlString = GlobalClass.DevlopmentApi+"catalog/?restaurant_id=\(GlobalClass.restaurantGlobalid)&category=Offers&status=ACTIVE"
                
            
            print(" category clicked menu api - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                   // print(response)

                                    if response.response?.statusCode == 200{
                                       
                                     let dict :NSDictionary = response.value! as! NSDictionary
                                     
                                        let list:NSArray = dict.value(forKey: "results") as! NSArray

                                        
                                        print(dict)
                                        
                                        
                                        if list.count == 0 {
                                           
                                    
                                            self.OffersList = []
                                            self.offertable.reloadData()

                                            ERProgressHud.sharedInstance.hide()
                                         
                                        self.showSimpleAlert(messagess: "No menu available in this category")
                                            
                                            
                                        }else{
                                            
                                            self.OffersList = list
                                            self.offertable.reloadData()
                                            
                                            ERProgressHud.sharedInstance.hide()

                                                }

                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {
                                        
                                self.showSimpleAlert(messagess:"No internet connection")
                                        
                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                        
                                self.showSimpleAlert(messagess:"Slow internet detected")
                                        
                                    }else{
                                    
                                        self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                }
                }
                
            }
    
    
    //MARK: Webservice Call for add to cart

     
    func addToCart() {
       
        print(passproductid)
        
        let defaults = UserDefaults.standard
        
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        if defaults.object(forKey: "AvlbCartId") == nil {
          
            self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
           
            
        }else{
        
         let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
            let cartidStr = String(avlCartId)
            
       // let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let admintoken = defaults.object(forKey: "custToken")as? String
            
            print("customer token -\(String(describing: admintoken))")
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

        let urlString = GlobalClass.DevlopmentApi+"cart-item/"
        
            let now = Date()

                let formatter = DateFormatter()

                formatter.timeZone = TimeZone.current

                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS" ///2020-10-19 12:05:10.625

                let dateString = formatter.string(from: now)
            
            print(dateString)
            
            let commentStr = ""
            
           print("commentStr - \(commentStr)")
            
            let cartdatacount = defaults.object(forKey: "cartdatacount")as! String
            
            let metadataDict = ["updated_at":dateString,"extra":commentStr, "quantity":quantityNumber as Int, "cart_id":avlCartId as Any,"product_id":passproductid as Any,"ingredient_id":0,"sequence_id":cartdatacount]
          
            let productarr = [metadataDict]
            print("product element Dict - \(productarr)")
            
            var ingredientarray = Array<Any>()
            ingredientarray.insert(metadataDict, at: 0)
            print("Array product with ingredient added - \(ingredientarray)")

            
            let fileUrl = URL(string: urlString)

            var request = URLRequest(url: fileUrl!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(autho, forHTTPHeaderField: "Authorization")
            request.setValue(customeridStr, forHTTPHeaderField: "user_id")
            request.setValue(cartidStr, forHTTPHeaderField: "cart_id")
            request.setValue("cart-item", forHTTPHeaderField: "action")


            request.httpBody = try! JSONSerialization.data(withJSONObject: ingredientarray)

            AF.request(request)
                .responseJSON { response in
                    // do whatever you want here
                    switch response.result {
                                            case .success:
                                                print(response)
                    
                                                if response.response?.statusCode == 201{
                    
                                                    ERProgressHud.sharedInstance.hide()
                    
                                                 let alert = UIAlertController(title: nil, message: "Product added successfully into the cart",         preferredStyle: UIAlertController.Style.alert)
                    
                    
                                                   alert.addAction(UIAlertAction(title: "OK",
                                                                                 style: UIAlertAction.Style.default,
                                                                                 handler: {(_: UIAlertAction!) in
                    
                                                                                    let home = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as! HomePage
                                                                                    self.navigationController?.pushViewController(home, animated: true)
                    
                                                   }))
                                                    
                                                    
                                                   self.present(alert, animated: true, completion: nil)
                                                    alert.view.tintColor = UIColor.black
                    
                                                }else{
                    
                                                    if response.response?.statusCode == 401{
                    
                                                        ERProgressHud.sharedInstance.hide()
                                                        self.SessionAlert()
                    
                                                    }else if response.response?.statusCode == 500{
                    
                                                        ERProgressHud.sharedInstance.hide()
                    
                                                        let dict :NSDictionary = response.value! as! NSDictionary
                    
                                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                    }else{
                    
                                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                       }
                    
                                                }
                    
                                                break
                                            case .failure(let error):
                                                ERProgressHud.sharedInstance.hide()
                    
                                                print(error.localizedDescription)
                    
                                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                    
                                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                    
                                                let msgrs = "URLSessionTask failed with error: The request timed out."
                    
                                                if error.localizedDescription == msg {
                    
                                            self.showSimpleAlert(messagess:"No internet connection")
                    
                                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                    
                                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                    
                                                }else{
                    
                                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                                }
                    
                                                   print(error)
                                            }
            }
            
            
        }

}
    
}

// MARK: - AlertController
extension OfferPage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}
