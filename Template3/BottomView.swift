//
//  BottomView.swift
//  Template3
//
//  Created by TISSA Technology on 2/16/21.
//

import UIKit

class BottomView: BottomPopupViewController {

    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    @IBOutlet weak var bottomViewshow: UIView!
    @IBOutlet weak var addresscity: UILabel!
    @IBOutlet weak var fulladdress: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        bottomViewshow.layer.cornerRadius = 20
        
        let defaults = UserDefaults.standard
        addresscity.text = defaults.object(forKey: "citylocation")as? String
        fulladdress.text = defaults.object(forKey: "addresslocation")as? String
        
    }
    
    
    
    
    @IBAction func dismissButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func locationlistBtnClicked(_ sender: Any) {
        
        
        let popupVC = (storyboard?.instantiateViewController(withIdentifier: "LocationViewlist") as? LocationViewlist)!
        
        present(popupVC, animated: true, completion: nil)
        
        
    }
    
    
    override var popupHeight: CGFloat { return height ?? CGFloat(300) }
    
    override var popupTopCornerRadius: CGFloat { return topCornerRadius ?? CGFloat(10) }
    
    override var popupPresentDuration: Double { return presentDuration ?? 1.0 }
    
    override var popupDismissDuration: Double { return dismissDuration ?? 1.0 }
    
    override var popupShouldDismissInteractivelty: Bool { return shouldDismissInteractivelty ?? true }
    
    override var popupDimmingViewAlpha: CGFloat { return BottomPopupConstants.kDimmingViewDefaultAlphaValue }
}
